import { useFonts } from 'expo-font';
import React from 'react';
import { StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import { lightTheme } from './src/constants/Theme';
import Navigation from './src/navigation/Navigation';
import configureStore from './src/store/configureStore';

const store = configureStore();

export default function App() {
  const [loaded] = useFonts({
    'Roboto-Bold': require('./assets/fonts/Roboto-Bold.ttf'),
    'Roboto-Italic': require('./assets/fonts/Roboto-Italic.ttf'),
    'Roboto-Regular': require('./assets/fonts/Roboto-Regular.ttf'),
    'OpenSans-Bold': require('./assets/fonts/OpenSans-Bold.ttf'),
    'OpenSans-SemiBold': require('./assets/fonts/OpenSans-SemiBold.ttf'),
    'MyriadPro-Light': require('./assets/fonts/MyriadPro-Light.otf'),
    'soria-font': require('./assets/fonts/soria-font.ttf'),
    'KeepCalm-Medium': require('./assets/fonts/KeepCalm-Medium.ttf'),
    'ProximaNovaAlt-Bold': require('./assets/fonts/ProximaNovaAltBold.otf'),
    'ProximaNova-Regular': require('./assets/fonts/ProximaNova-Regular.otf'),
    'ProximaNova-Bold': require('./assets/fonts/ProximaNovaBold.otf'),
    'Avante-Granadd': require('./assets/fonts/Avgardd.ttf'),
    'Avante-Granatddo': require('./assets/fonts/Avgarddo.ttf'),
    'Avante-Granatdm': require('./assets/fonts/Avgardm.ttf'),
    'Avante-Granatdn': require('./assets/fonts/Avgardn.ttf'),
    'Avante-Granatdni': require('./assets/fonts/Avgardni.ttf'),
    'CaviarDreams-Bold': require('./assets/fonts/CaviarDreams_Bold.ttf'),
    'CaviarDreams-Regu': require('./assets/fonts/CaviarDreams.ttf'),
  });

  if (!loaded) {
    return null;
  }
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: lightTheme.PRIMARY_BACKGROUND_COLOR,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
