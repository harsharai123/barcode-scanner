import { Dimensions } from 'react-native';

export const DIMENSIONS = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width,
};

export const SIZES = {
  // global sizes
  base: 8,
  font: 14,
  radius: 20,
  textInputBorderRadius: 25,
  padding: 10,
  padding2: 15,

  // font sizes
  largeTitle: 50,
  h1: 30,
  h2: 22,
  h3: 20,
  h4: 18,
  h5: 14,
  h6: 16,
  body1: 30,
  body2: 20,
  body3: 16,
  body4: 14,
  body5: 12,

  // app dimensions
  // width,
  // height,
};

export const FONTS = {
  largeTitle: {
    fontFamily: 'ProximaNovaAlt-Bold',
    fontSize: SIZES.largeTitle,
    lineHeight: 55,
  },
  h1: { fontFamily: 'ProximaNovaAlt-Bold', fontSize: SIZES.h1, lineHeight: 36 },
  h2: { fontFamily: 'Roboto-Bold', fontSize: SIZES.h2, lineHeight: 30 },
  h3: { fontFamily: 'Roboto-Bold', fontSize: SIZES.h3, lineHeight: 22 },
  h4: { fontFamily: 'ProximaNova-Bold', fontSize: SIZES.h4, lineHeight: 20 },
  h7: { fontFamily: 'ProximaNova-Bold', fontSize: SIZES.h6, lineHeight: 20 },
  h5: { fontFamily: 'ProximaNova-Regular', fontSize: SIZES.h5, lineHeight: 20 },
  h6: { fontFamily: 'AvenirLTStd-Black', fontSize: SIZES.h3, lineHeight: 22 },
  body1: {
    fontFamily: 'Roboto-Regular',
    fontSize: SIZES.body1,
    lineHeight: 36,
  },
  body2: {
    fontFamily: 'ProximaNova-Bold',
    fontSize: SIZES.body4,
    lineHeight: 22,
  },
  body3: {
    fontFamily: 'Roboto-Regular',
    fontSize: SIZES.body3,
    lineHeight: 22,
  },
  body4: {
    fontFamily: 'Roboto-Regular',
    fontSize: SIZES.body4,
    lineHeight: 22,
  },
  body5: {
    fontFamily: 'ProximaNova-Regular',
    fontSize: SIZES.body5,
    lineHeight: 22,
  },
};

export const darkTheme = {
  mode: 'dark',
  PRIMARY_BACKGROUND_COLOR: '#282828',
  PRIMARY_TEXT_COLOR: '#767d92',
  SECONDARY_TEXT_COLOR: '#ffffff',
  PRIMARY_BUTTON_COLOR: '#152642',
  SECONDARY_BUTTON_COLOR: '#506680',
};
export const lightTheme = {
  mode: 'light',
  PRIMARY_BACKGROUND_COLOR: '#CCCCCC',
  SECONDARY_COLOR: '#3366FF',
  BLACK_COLOR: '#282828',
  ACCENT_COLOR: '009FE3',
  PRIMARY_TEXT_COLOR: '#CCCCCC',
  SECONDARY_TEXT_COLOR: '#A9A9A9',
  PRIMARY_BUTTON_COLOR: '#3366FF',
  SECONDARY_BUTTON_COLOR: '#a1c9f1',
  WHITE_COLOR: '#FFF',
  // SECONDARY_COLOR: '#009FE3',
};

const appTheme = { lightTheme, darkTheme, FONTS, DIMENSIONS, SIZES };

export default appTheme;
