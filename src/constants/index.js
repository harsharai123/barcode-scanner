import { lightTheme, darkTheme, FONTS, DIMENSIONS, SIZES } from "./Theme";

export { lightTheme, darkTheme, FONTS, DIMENSIONS, SIZES };
