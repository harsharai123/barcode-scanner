import { ADD_PRODUCT, CLEAR_CART, REMOVE_FROM_CART, UPDATE_CART_QUANTITY, UPDATE_ITEM_PRICE, UPDATE_TAX, UPDATE_TOTAL_PRICE } from '../constants/index';

const initialState = { cart: [], subtotal: 0, tax: 0, total: 0 };

const findTotalPrice = cart => cart.reduce((res, item) => res + item.quantity * item.mrp, 0);
const findTaxData = cart => cart.reduce((res, item) => res + item.quantity * item.taxprice, 0);

// var n = num.toFixed(2);
const cartReducer = (state = initialState, action) => {
  let cart = state.cart;
  let subtotal = 0;
  let tax = 0;
  let total = 0;
  switch (action.type) {
    case ADD_PRODUCT:
      cart.push(action.payload);
      total = findTotalPrice(cart);
      tax = findTaxData(cart);
      subtotal = total - tax;
      return { ...state, cart, subtotal, tax, total };
    case CLEAR_CART:
      return { ...state, cart: [], subtotal: 0, tax: 0, total: 0 };
    case UPDATE_CART_QUANTITY:
      const index = cart.findIndex(ele => ele.barCodeId === action.payload.productId);
      if (index === -1 || action.payload.quantity < 0) return state;
      cart[index].quantity = action.payload.quantity;
      total = findTotalPrice(cart);
      tax = findTaxData(cart);
      subtotal = total - tax;
      return { ...state, cart, subtotal, tax, total };
    case REMOVE_FROM_CART:
      cart = cart.filter(ele => ele.barCodeId !== action.payload.productId);
      total = findTotalPrice(cart);
      tax = findTaxData(cart);
      subtotal = total - tax;
      return { ...state, cart, subtotal, tax, total };
    case UPDATE_ITEM_PRICE:
      return { ...state, subtotal: findTotalPrice(action.payload) };
    case UPDATE_TAX:
      return { ...state, tax: findTaxData(action.payload) };
    case UPDATE_TOTAL_PRICE:
      return { ...state, total: action.payload.subtotal + action.payload.tax };
    default:
      return state;
  }
};
export default cartReducer;
