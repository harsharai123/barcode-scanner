import { GET_ITEMS, UPDATE_ITEM } from '../constants/index';

const initialState = [];
const itemReducer = (state = initialState, action) => {
  let items = state;
  switch (action.type) {
    case GET_ITEMS:
      return action.payload;
    case UPDATE_ITEM:
      console.log('STOCKK', action);
      const index = items.findIndex(ele => ele.itemCode === action.payload.itemCode);

      if (index === -1) return state;
      const item = items[index];
      item.barCodeId = action.payload.barCodeId;
      item.stock = action.payload.stock;
      item.enableButton = false;
      item.isBarCodeCreated = true;
      items.push(item);
      items.splice(index, 1);
      console.log('LENGTH', items.length);

      return items;
    default:
      return state;
  }
};
export default itemReducer;
