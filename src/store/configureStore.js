import { combineReducers, createStore } from 'redux';
import cartReducer from '../store/reducers/cartReducers';
import itemReducer from '../store/reducers/itemReducers';
import userReducer from '../store/reducers/userReducer';
const rootReducer = combineReducers({ cart: cartReducer, items: itemReducer, userDetails: userReducer });
const configureStore = () => {
  return createStore(rootReducer);
};
export default configureStore;
