// import { ADD_PRODUCT, REMOVE_FROM_CART } from "../constants/index";

// export const addToCart = (product) => {
//   return {
//     type: ADD_PRODUCT,
//     payload: {
//       product,
//       // quantity: 1
//     },
//   };
// };
// export const removeFromCart = (productId) => {
//   return {
//     type: REMOVE_FROM_CART,
//     payload: {
//       productId: productId,
//     },
//   };
// };

import { ADD_PRODUCT, CLEAR_CART, REMOVE_FROM_CART, UPDATE_ITEM_PRICE, UPDATE_TAX, UPDATE_TOTAL_PRICE } from '../constants';
export function addProduct(product) {
  return {
    type: ADD_PRODUCT,
    payload: product,
  };
}
export const updateCartQuantity = (productId, quantity) => {
  return {
    type: 'UPDATE_CART_QUANTITY',
    payload: {
      productId,
      quantity: quantity,
    },
  };
};

export const updateItemPrice = product => {
  return {
    type: UPDATE_ITEM_PRICE,
    payload: product,
  };
};

export const updateTax = product => {
  return {
    type: UPDATE_TAX,
    payload: product,
  };
};

export const totalPrice = (subtotal, tax) => {
  return {
    type: UPDATE_TOTAL_PRICE,
    payload: {
      subtotal: subtotal,
      tax: tax,
    },
  };
};

export const removeFromCart = productId => {
  return {
    type: REMOVE_FROM_CART,
    payload: {
      productId: productId,
    },
  };
};
export const clearCart = () => {
  return {
    type: CLEAR_CART,
  };
};
