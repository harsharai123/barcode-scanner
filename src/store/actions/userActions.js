import { LOGIN } from '../constants';
export function updateUserDetails(details) {
  return {
    type: LOGIN,
    payload: details,
  };
}
