import { GET_ITEMS, UPDATE_ITEM } from '../constants';
export function getItems(items) {
  return {
    type: GET_ITEMS,
    payload: items,
  };
}
export function updateItem(itemCode, barCodeId, stock) {
  return {
    type: UPDATE_ITEM,
    payload: { itemCode, barCodeId, stock, enableButton: false },
  };
}
