import { Ionicons } from "@expo/vector-icons";
import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import {
  createBottomTabNavigator,
  createMaterialTopTabNavigator,
} from "react-navigation-tabs";
import AllItems from "../../src/container/AllItems";
import HomeScreen from "../../src/container/HomeScreen";
import LoginScreen from "../component/Authentication/LoginScreen";
import TabBar from "../component/bottomTab/BottomTab";
import AddItemsBarcodeScan from "../component/scanner/AddItemsBarcodeScan";
import { lightTheme } from "../constants/Theme";
import AddStocks from "../container/AddStocks";
import BillScreen from "../container/BillScreen";
import CreateBarcode from "../container/CreateBarcode";
import CustomerDetails from "../container/CustomerDetails";
import EditProfile from "../container/EditProfie";
import GetItems from "../container/GetItems";
import PrivacyPolicy from "../container/PrivacyPolicy";
import ProfileScreen from "../container/ProfileScreen";
import Reports from "../container/Reports";
import Scanner from "../container/Scanner";
import TodaysBill from "../container/TodaysBill";
import UpdateStock from "../container/UpdateStock";
import SplashScreen from "../container/splashscreen";

// import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
// const ReportScreenStack = createStackNavigator({});
const AuthLoadingStack = createStackNavigator(
  {
    SplashScreen: { screen: SplashScreen },
  },
  {
    // initialRouteName: RouteName.START_UP,
    headerMode: "none",
  }
);

const TabStack = createMaterialTopTabNavigator(
  {
    GetItems: {
      screen: GetItems,
      navigationOptions: ({ navigation }) => ({
        headerLeft: () => null,
        title: "ADD BARCODE",
        headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
      }),
    },

    AddStocks: {
      screen: AddStocks,
      navigationOptions: ({ navigation }) => ({
        headerLeft: () => null,
        title: "ADD STOCKS",

        headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
      }),
    },
  },

  {
    tabBarOptions: {
      activeTintColor: "#CCCCCC",
      // showLabel: false,
      inactiveTintColor: "#CCCCCC",
      style: {
        backgroundColor: lightTheme.BLACK_COLOR,
      },
    },
  }
);
const HomeScreenStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        color: "#2e64e5",
      },

      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: "#282828",
      },
    }),
  },
  Reports: {
    screen: Reports,
    navigationOptions: ({ navigation }) => ({
      headerShown: false,
    }),
  },

  TabStack: {
    screen: TabStack,
    navigationOptions: ({ navigation }) => {
      return {
        headerShown: false,
      };
    },
  },

  TodaysBill: {
    screen: TodaysBill,
    navigationOptions: ({ navigation }) => ({
      headerShown: false,
    }),
  },
  AddItemsBarcodeScan: {
    screen: AddItemsBarcodeScan,
    navigationOptions: ({ navigation }) => {
      return {
        headerShown: true,
      };
    },
  },
  UpdateStock: {
    screen: UpdateStock,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        color: "#2e64e5",
        fontSize: 18,
      },
      headerLeft: () => null,
      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: lightTheme.BLACK_COLOR,
        borderWidth: 1,
        borderBottomColor: lightTheme.WHITE_COLOR,
      },
      headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
    }),
  },
  CreateBarcode: {
    screen: CreateBarcode,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        color: "#2e64e5",
        fontSize: 18,
      },
      headerLeft: () => null,
      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: lightTheme.BLACK_COLOR,
        borderWidth: 1,
        borderBottomColor: lightTheme.WHITE_COLOR,
      },
      headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
    }),
  },

  // Reports: ReportScreenStack,
});
const ProfileScreenStack = createStackNavigator({
  ProfileScreen: {
    screen: ProfileScreen,
    navigationOptions: ({ navigation }) => ({
      title: "Profile",
      headerTitleStyle: {
        color: "#2e64e5",
        fontSize: 18,
      },

      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: lightTheme.BLACK_COLOR,
      },
      headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
    }),
  },
  PrivacyPolicy: {
    screen: PrivacyPolicy,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        color: lightTheme.SECONDARY_COLOR,
        fontSize: 18,
      },
      headerLeft: () => null,
      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: lightTheme.BLACK_COLOR,
      },

      headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
    }),
  },
  // Support: {
  //   screen: Support,
  //   navigationOptions: ({ navigation }) => ({
  //     headerTitleStyle: {
  //       color: lightTheme.SECONDARY_COLOR,
  //       fontSize: 18,
  //     },

  //     headerStyle: {
  //       shadowColor: '#FFF',
  //       elevation: 0,
  //       backgroundColor: '#FFF',
  //     },
  //   }),
  // },

  EditProfile: {
    screen: EditProfile,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {},
      headerLeft: () => null,
      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,

        backgroundColor: lightTheme.BLACK_COLOR,
      },
    }),
  },
});

const BillScreenStack = createStackNavigator({
  BillScreen: {
    screen: BillScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        color: "#2e64e5",
        fontSize: 18,
      },

      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: lightTheme.BLACK_COLOR,
      },
      headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
    }),
  },
  Scanner: {
    screen: Scanner,
    navigationOptions: ({ navigation }) => {
      return {
        headerShown: true,
      };
    },
  },
  CustomerDetails: {
    screen: CustomerDetails,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        color: "#2e64e5",
        fontSize: 18,
      },
      headerLeft: () => null,
      headerStyle: {
        shadowColor: "#FFF",
        elevation: 0,
        backgroundColor: lightTheme.BLACK_COLOR,
        borderWidth: 1,
        borderBottomColor: lightTheme.WHITE_COLOR,
      },
      headerTintColor: lightTheme.PRIMARY_TEXT_COLOR,
    }),
  },
});

const ScannerStack = createStackNavigator({
  Scanner: {
    screen: Scanner,
    navigationOptions: ({ navigation }) => {
      return {
        headerShown: false,
      };
    },
  },
});
const AllItemsStack = createStackNavigator({
  AllItems: {
    screen: AllItems,
    navigationOptions: ({ navigation }) => {
      return {
        headerShown: false,
      };
    },
  },
});
AllItemsStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName;

  if (routeName == "") {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

ScannerStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName;

  if (routeName == "Scanner") {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const TabScreens = createBottomTabNavigator(
  {
    Home: HomeScreenStack,
    Brand: BillScreenStack,
    Bag: ScannerStack,
    AllItem: AllItemsStack,
    Profile: ProfileScreenStack,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        if (routeName === "Home") {
          iconName = `home`;
          return (
            <Ionicons
              name={iconName}
              size={horizontal ? 20 : 25}
              color={tintColor}
            />
          );
        } else if (routeName === "Brand") {
          iconName = `cart`;
          return (
            <Ionicons
              name={iconName}
              size={horizontal ? 20 : 25}
              color={tintColor}
            />
          );
        } else if (routeName === "Bag") {
          iconName = `scan`;
          return (
            <Ionicons
              name={iconName}
              size={horizontal ? 20 : 25}
              color={tintColor}
            />
          );
        } else if (routeName === "AllItem") {
          iconName = `receipt`;
          return (
            <Ionicons
              name={iconName}
              size={horizontal ? 20 : 25}
              color={tintColor}
            />
          );
        } else if (routeName === "Profile") {
          iconName = `person-circle-outline`;
          return <Ionicons name={iconName} size={32} color={tintColor} />;
        }

        // return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarComponent: (props) => (
      <TabBar {...props} showLabel={false} style={{ height: 60 }} />
    ),
    tabBarOptions: {
      tabFeatured: "Bag",

      backgroundFeaturedIcon: lightTheme.PRIMARY_TEXT_COLOR,
      activeFeaturedTintColor: "black",
      inactiveFeatureTintColor: lightTheme.PRIMARY_BUTTON_COLOR,

      showLabel: true,
      activeTintColor: "#3366FF",
      inactiveTintColor: "#CCCCCC",
      style: {
        height: 80,
        backgroundColor: "#282828",
        borderTopWidth: 1,
        borderTopColor: "#F2F3EF",
      },
      tabStyle: {},
    },
  }
);

const App = createSwitchNavigator({
  SplashScreen: {
    screen: AuthLoadingStack,
  },
  Login: {
    screen: LoginScreen,
  },

  Auth: {
    screen: TabScreens,
  },
  //   HomeStack: {
  //     screen: HomeBottomTabNavigator,
  //   },
  //   SearchStack: {
  //     screen: SearchNavigator,
  //   },
  // Saved: {
  //   screen: SavedNavigator,
  // },
});

export default createAppContainer(App);
