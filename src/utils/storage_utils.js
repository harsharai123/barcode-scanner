import AsyncStorage from "@react-native-async-storage/async-storage";

export const _storeData = async (key, data) => {
  try {
    console.log("Stored success", key, data);
    await AsyncStorage.setItem(key, data);
  } catch (e) {
    console.log("\nError while saving data");
  }
};
export const _retrieveData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
    return null;
  } catch (error) {
    // Error retrieving data
    console.log(`Error: Getting ${key} value rom storage`);
    throw new Error(`Error: Getting ${key} value rom storage`);
  }
};
export const _removeData = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    console.log("\nError while deleting data");
  }
};
