import { lightTheme } from '../constants';

export const DATA = [
  {
    name: 'SCANNER',
    image: require('../../assets/images/barcode-scanner.png'),
    id: 1,
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
  },
  {
    name: 'ALL ITEMS',
    image: require('../../assets/images/Add-items.png'),
    id: 2,
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
  },
  {
    name: 'REPORTS',
    image: require('../../assets/images/report.png'),
    id: 3,
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
  },
  {
    name: 'TRANSACTION',
    image: require('../../assets/images/resolved.png'),
    id: 4,
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
  },
];
