import React from 'react';
import { ActivityIndicator, Modal, StyleSheet, View } from 'react-native';

const FullScreenLoader = () => (
  <Modal transparent={true} animationType={'none'} visible={true}>
    <View style={styles.modalBackground}>
      <View style={styles.activityIndicatorWrapper}>
        <ActivityIndicator size='large' color='black' animating />
      </View>
    </View>
  </Modal>
);

export default FullScreenLoader;
const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  activityIndicatorWrapper: {
    backgroundColor: 'grey',
    height: 82,
    width: 82,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader: {
    color: 'black',
    height: 50,
    width: 50,
  },
});
