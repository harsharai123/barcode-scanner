import { FontAwesome5, MaterialCommunityIcons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import AWS from "aws-sdk";
import { decode } from "base64-arraybuffer";
import * as FileSystem from "expo-file-system";
import * as Print from "expo-print";
import * as Sharing from "expo-sharing";
import * as SMS from "expo-sms";
import moment from "moment";
import React, { Component } from "react";
import { Linking, StyleSheet, Text, View } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { addBill } from "../../../NewServices";
import { FONTS, lightTheme } from "../../constants";
const errPopup = (err) =>
  isNeedErrorPopup &&
  Alert.alert("Error", err.message, [
    {
      text: "Cancel",
      onPress: () => console.log("Cancel Pressed"),
      style: "cancel",
    },
  ]);
class Invoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProducts: this.props.cart,
      total: this.props.total,
      userData: {},
    };
  }

  componentDidMount = async () => {
    var unix_seconds = new Date().getTime() / 1000;
    const val = await AsyncStorage.getItem("logincredential");
    const obj = JSON.parse(val);
    this.setState({ userData: obj });
    console.log("USERDATAAAA", obj);
    // console.log('Invoice id printed', unix_seconds);
    const dateToday = new Date(); // Mon Jun 08 2020 16:47:55 GMT+0800 (China Standard Time)
    const timestamp = this.epoch(dateToday);
    console.log("Invoice id printed", timestamp);
    this.setState({ date: timestamp });
  };
  epoch = (date) => {
    return Date.parse(date);
  };

  contentToHtml = () =>
    ` <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />


            <!-- Favicon -->
            <link rel="icon" href="./images/soda.jpg" type="image/x-icon" />

            <!-- Invoice styling -->
            <style>
                body {
                    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                    text-align: center;
                    color: #777;
                }

                body h1 {
                    font-weight: 300;
                    margin-bottom: 0px;
                    padding-bottom: 0px;
                    color: #000;
                }

                body h3 {
                    font-weight: 300;
                    margin-top: 10px;
                    margin-bottom: 20px;
                    font-style: italic;
                    color: #555;
                }

                body a {
                    color: #06f;
                }

                .invoice-box {
                    max-width: 800px;
                    margin: auto;
                    padding: 30px;
                    border: 1px solid #eee;
                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                    font-size: 16px;
                    line-height: 24px;
                    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                    color: #555;
                }

                .invoice-box table {
                    width: 100%;
                    line-height: inherit;
                    text-align: left;
                    border-collapse: collapse;
                }

                .invoice-box table td {
                    padding: 5px;
                    vertical-align: top;
                }

                .invoice-box table tr td:nth-child(2) {
                    text-align: right;
                }

                .invoice-box table tr.top table td {
                    padding-bottom: 20px;
                }

                .invoice-box table tr.top table td.title {
                    font-size: 45px;
                    line-height: 45px;
                    color: #333;
                }

                .invoice-box table tr.information table td {
                    padding-bottom: 40px;
                }

                .invoice-box table tr.heading td {
                    background: #eee;
                    border-bottom: 1px solid #ddd;
                    font-weight: bold;
                }

                .invoice-box table tr.details td {
                    padding-bottom: 20px;
                }

                .invoice-box table tr.item td {
                    border-bottom: 1px solid #eee;
                }

                .invoice-box table tr.item.last td {
                    border-bottom: none;
                }

                .invoice-box table tr.total td:nth-child(1) {
                    border-top: 2px solid #eee;
                    font-weight: bold;
                }

                @media only screen and (max-width: 600px) {
                    .invoice-box table tr.top table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }

                    .invoice-box table tr.information table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                }
            </style>
        </head>
         <body>
            <div class="invoice-box">
                <table>
                    <tr class="top">
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td class="title">
                                    Roupya
                                    </td>

                                    <td>

                                        Created: ${moment().format(
                                          "DD/MM/yyyy"
                                        )}<br />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="information">
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        Sparksuite, Inc.<br />
                                        12345 Sunny Road<br />
                                        Sunnyville, TX 12345
                                    </td>

                                    <td>
                                        Acme Corp.<br />
                                        ${this.props.data.customername}<br />
                                        ${this.props.data.customeremail}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="heading">
                        <td>Item</td>
                        <td>Quantity</td>
                        <td>Price</td>
                        <td>Tax</td>
                    </tr>

                    ${(this.state.selectedProducts || []).map((item, id) => {
                      return ` <tr class="item">
                                <td>${item.itemName}</td>
                                <td>${item.quantity}</td>
                                 <td>${item.totalEstimatedAmountExcludingGst}</td>
                                 <td>${item.totalGstInAmount}</td>
                            </tr>`;
                    })}
                    <tr class="total">
                        <td></td>

                        <td>Total: ${this.props.data.total}</td>
                    </tr>
                </table>
            </div>
        </body>
    </html>`;
  strip(html) {
    let doc = new DOMParser().parseFromString(html, "text/html");
    return doc.body.textContent || "";
  }
  dataToDatabase = async () => {
    try {
      await addBill({
        id: 2,
        customerName: this.props.data.customername,
        customerPhone: this.props.data.customerphone,
        invoiceId: this.state.date,
        invoiceUrl: "",
        billGeneratedDate: "2021-05-30T14:44:44",
        billAmount: this.props.data.total,
        userId: 1,
        userName: this.state.userData?.firstName || "",
      });

      // const data = (this.state.selectedProducts || []).map(e => {
      //   return {
      //     itemCode: e.itemCode,
      //     stock: e.quantity,
      //   };
      // });
      // await lessenStock(data);
    } catch (err) {
      console.log("billed", err);
    }
  };

  sendSms = async () => {
    this.dataToDatabase();

    const { result } = await SMS.sendSMSAsync(
      [this.props.data.customerphone],
      `  ${(this.state.selectedProducts || []).map((item, id) => {
        return `
        ROUPYA
Item Name: ${item.itemName}
Item Quantity: ${item.quantity}
 MRP: ${item.totalEstimatedAmountExcludingGst}
 Tax :${item.totalGstInAmount}
 `;
      })}
 Total: ${this.props.data.total}
`
    );
  };
  fileToBase64 = async (uri) => {
    console.log("Entered in base 64", uri);
    try {
      const content = await FileSystem.readAsStringAsync(uri, {
        encoding: FileSystem.EncodingType.Base64,
      });
      console.log("CONTENt checked", content);
      return content;
    } catch (e) {
      console.warn("fileToBase64()", e.message);
      return "";
    }
  };
  async execute(type) {
    try {
      const html = this.contentToHtml();
      this.dataToDatabase();

      // console.log('BILL', response);
      const { uri } = await Print.printToFileAsync({ html });
      console.log("pdf", uri);
      const val = await Sharing.shareAsync(uri);
      this.props.onClose(val);
      console.log("CHECK");

      const s3bucket = new AWS.S3({
        accessKeyId: "AKIAUHWYCSMEZBZH4NXN",
        secretAccessKey: "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",
        Bucket: " invoiceurl",
        signatureVersion: "v4",
      });
      console.log("S3 bucket", s3bucket);

      let fPath = uri;

      let encodedValue = await this.fileToBase64(fPath);
      console.log("Encodedvalue", encodedValue);
      const fileName = String(Date.now());
      let contentDeposition = 'inline;filename="' + fileName + '"';

      let decodedValue = decode(encodedValue);
      console.log("decodedValue", decodedValue);
      let ContentType = "image/jpg";
      const params = {
        Bucket: bucket_address,
        Key: fileName,
        Body: decodedValue,
        ContentDisposition: contentDeposition,
        ContentType: ContentType,
      };
      console.log("CHECKKKKKK", params);
      s3bucket.upload(params, (err, data) => {
        if (err) {
          console.log("error in callback");
          console.log(err);
        } else {
          console.log("S# BUCKET SUCCESS", data);
        }
      });
    } catch (err) {
      console.log(err.message);
    }
  }
  sendWhatsApp = async () => {
    this.dataToDatabase();
    let msg = `  ${(this.state.selectedProducts || []).map((item, id) => {
      return `
      ROUPYA
Item Name: ${item.itemName}
Item Quantity: ${item.quantity}
MRP: ${item.mrp}
Tax :${item.taxprice}
`;
    })}
Total: ${this.props.total}
`;
    let phoneWithCountryCode = "+91" + this.props.data.customerphone;

    let mobile =
      Platform.OS == "ios" ? phoneWithCountryCode : "+" + phoneWithCountryCode;
    if (mobile) {
      if (msg) {
        let url = "whatsapp://send?text=" + msg + "&phone=" + mobile;
        Linking.openURL(url)
          .then((data) => {
            console.log("WhatsApp Opened");
          })
          .catch(() => {
            alert("Make sure WhatsApp installed on your device");
          });
      } else {
        alert("Please insert message to send");
      }
    } else {
      alert("Please insert mobile no");
    }
  };
  render() {
    const { userData } = this.state;
    console.log("ITEMDATA", this.state.selectedProducts);
    return (
      <View>
        <TouchableHighlight
          style={{
            marginTop: 20,
            shadowColor: lightTheme.WHITE_COLOR,
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 8,
            elevation: 5,
            width: "40%",
            height: "10%",
            padding: 20,
            borderRadius: 10,
            alignSelf: "center",
            justifyContent: "center",
            backgroundColor: lightTheme.SECONDARY_COLOR,
          }}
          onPress={() => this.execute(this.state.selectedProducts)}
        >
          <Text
            style={{
              ...FONTS.h5,
              textAlign: "center",
              color: lightTheme.WHITE_COLOR,
            }}
          >
            Send Invoice
          </Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={{
            marginTop: 20,
            shadowColor: lightTheme.WHITE_COLOR,
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 8,
            elevation: 5,
            width: "20%",
            height: "10%",
            padding: 20,
            borderRadius: 10,
            alignItems: "center",
            alignSelf: "center",
            justifyContent: "center",
            backgroundColor: lightTheme.SECONDARY_COLOR,
          }}
          onPress={() => this.sendSms()}
        >
          <MaterialCommunityIcons
            name="message-settings-outline"
            size={24}
            color="white"
          />
        </TouchableHighlight>
        <TouchableHighlight
          style={{
            marginTop: 20,
            shadowColor: "	#075E54",
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 8,
            elevation: 5,
            width: "20%",
            height: "10%",
            padding: 20,
            borderRadius: 10,
            alignItems: "center",
            alignSelf: "center",
            justifyContent: "center",
            backgroundColor: "#075E54",
          }}
          onPress={() => this.sendWhatsApp()}
        >
          <FontAwesome5 name="whatsapp" size={24} color="white" />
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => ({
  cart: state.cart.cart,
  total: state.cart.total,
});
export default connect(mapStateToProps)(Invoice);
