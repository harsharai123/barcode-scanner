import { Dimensions } from 'react-native';
import { FONTS, lightTheme } from '../../constants';

const { width, height } = Dimensions.get('window');
export default {
  cardContainer: {
    flex: 1,
    backgroundColor: '#CCCCCC',
    margin: 15,
    borderRadius: 25,
    borderWidth: 5,
    shadowColor: 'black',
    shadowOffset: { width: 2, height: 3 },
    shadowOpacity: 0.2,
    elevation: 10,
    // borderShadowOpacity: 0.8,
    // borderShadowRadius: 2,
    borderColor: 'white',
    paddingVertical: 80,
  },
  cardImageContainer: {},
  cardImageStyles: {
    width: 180,
    height: 180,
    alignSelf: 'center',
    // backgroundColor: lightTheme.SECONDARY_COLOR,
  },
  textStyles: {
    textAlign: 'center',
    ...FONTS.h4,
    color: lightTheme.BLACK_COLOR,
  },
};
