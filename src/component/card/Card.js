import PropTypes from 'prop-types';
import React from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import styles from './CardStyles';
const { height, width } = Dimensions.get('window');

const Card = props => {
  return (
    <TouchableOpacity style={{ ...styles.cardContainer, ...props.cardContainer }} onPress={props.onPressCard}>
      {/* <MaterialCommunityIcons name="user" size={24} color="black" /> */}
      {props.image && (
        <View style={{ ...styles.cardImageContainer, ...props.cardImageContainer }}>
          <Image source={props.image} style={{ ...styles.cardImageStyles, ...props.imageStyles }} />
        </View>
      )}
      <Text style={{ ...styles.textStyles, ...props.textStyles }}>{props.cardText}</Text>
    </TouchableOpacity>
  );
};

export default Card;

Card.propTypes = {
  cardText: PropTypes.string,
};

Card.defaultProps = {
  cardText: 'Scanned',
};
