import { Dimensions } from 'react-native';
import { lightTheme } from '../../constants';

const { width, height } = Dimensions.get('window');
export default {
  cardContainer: {
    flex: 1,
    backgroundColor: '#F8F8F8',
    margin: 15,
    borderRadius: 25,
    paddingVertical: 80,
  },
  billcardImageStyles: {
    width: 80,
    height: 80,
  },
  cartCard: {
    height: 330,
    elevation: 15,
    borderRadius: 10,
    backgroundColor: '#FFF',
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    flexDirection: 'row',
    //  alignItems: "center",
  },
  actionBtn: {
    width: 80,
    height: 30,
    backgroundColor: lightTheme.BLACK_COLOR,
    borderRadius: 30,
    paddingHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  topLeftContainer: {
    height: 100,
    marginLeft: 10,
    paddingVertical: 20,
    flex: 1,
  },
};
