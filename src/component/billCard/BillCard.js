import { MaterialIcons } from '@expo/vector-icons';
import React from 'react';
import { Dimensions, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { FONTS, lightTheme } from '../../constants/Theme';
import { removeFromCart, updateCartQuantity } from '../../store/actions/cartActions';
import styles from './BillCardStyles';
const { height, width } = Dimensions.get('window');

const connected = comp =>
  connect(state => ({ cart: state.cart.cart }), {
    updateCartQuantity,
    removeFromCart,
  })(comp);

const BillCard = props => (
  <View>
    {props.cart.map((item, id) => (
      <ScrollView key={id}>
        <View style={styles.cartCard}>
          <View style={styles.topLeftContainer}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ ...FONTS.h7, marginBottom: 5, color: '#282828' }}>{item.itemName}</Text>
              <Text style={{ ...FONTS.h5, marginBottom: 5, marginLeft: 15, color: '#282828' }}>
                {item.metal} - {item.purity}
              </Text>
            </View>
            {/* <Text style={{ fontSize: 13, color: '#A9A9A9' }}>{item.taxprice}</Text> */}
            {/* <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  ...FONTS.h4,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                ₹
              </Text>
              <TextInput style={{ ...FONTS.h4, justifyContent: 'center' }} color={lightTheme.BLACK_COLOR} onChangeText={value => props.changebillcardValue(value, item)} value={JSON.stringify(item.mrp)} />
            </View> */}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Weight
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.weight} gm {/* {props.otherCharges} */}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Total Pieces
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'left',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.totalPieces}
              </Text>
            </View>
            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Metal
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.metal} - {item.purity}
              </Text>
            </View> */}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Metal Rate
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                ₹ {item.metalRate} / gm
              </Text>
            </View>
            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Purity :
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.purity}
              </Text>
            </View> */}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'left',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                MC Type/ Rate
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.mcType} - ₹ {item.mcRate} / gm
              </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Other Charges
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                ₹ {item.otherCharges1 + item.otherCharges2}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Other Charges 1 Desc
              </Text>

              <View style={{ flexDirection: 'column' }}>
                <Text style={{ ...FONTS.h5, textAlign: 'left', alignSelf: 'center' }} color={lightTheme.BLACK_COLOR}>
                  {' '}
                  {item.otherCharges1Desc}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Other Charges 2 Desc
              </Text>

              <View style={{ flexDirection: 'column' }}>
                <Text style={{ ...FONTS.h5, textAlign: 'left', alignSelf: 'center' }} color={lightTheme.BLACK_COLOR}>
                  {' '}
                  {item.otherCharges2Desc}
                  {/* {'+' + stock} */}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Other Charges 3 desc
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.otherCharges3Desc}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                GST in Percentage
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                {item.metalGstInPercentage} %
              </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                GST in Amount
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                ₹ {item.totalGstInAmount}
              </Text>
            </View>
            <View
              style={{
                marginTop: 8,
                marginBottom: 8,
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Estimated Amount Excluding GST
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                ₹ {item.totalEstimatedAmountExcludingGst}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                Estimated Amount Including GST
              </Text>
              <Text
                style={{
                  ...FONTS.h5,
                  textAlign: 'center',
                  alignSelf: 'center',
                }}
                color={lightTheme.BLACK_COLOR}>
                {' '}
                ₹ {item.totalEstimatedAmountIncludingGst}
              </Text>
            </View>
          </View>
          <View style={{ marginRight: -10, marginTop: -14, alignItems: 'center' }}>
            <Text style={{ ...FONTS.h4 }}> </Text>
            {/* <View style={styles.actionBtn}>
              <AntDesign style={{ marginRight: 7 }} name='minus' size={18} color={lightTheme.WHITE_COLOR} onPress={() => props.updateCartQuantity(item.barCodeId, item.quantity - 1)} />
              <Text
                style={{
                  ...FONTS.h4,
                  marginRight: 7,
                  color: lightTheme.WHITE_COLOR,
                }}>
                {item.quantity}
              </Text>
              <AntDesign name='plus' size={18} color={lightTheme.WHITE_COLOR} onPress={() => props.updateCartQuantity(item.barCodeId, item.quantity + 1)} />
            </View> */}

            <MaterialIcons name='delete' size={24} color='red' onPress={() => props.removeFromCart(item.barCodeId)} />
          </View>
        </View>
      </ScrollView>
    ))}
  </View>
);

export default connected(BillCard);
