import { BarCodeScanner } from 'expo-barcode-scanner';
import React, { Component } from 'react';
import { Animated, Button, Dimensions, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { putBarcodeId } from '../../../NewServices';
import { updateItem } from '../../store/actions/itemActions';
const { width, height } = Dimensions.get('window');

const connected = comp => connect(state => ({}), { updateItem })(comp);
class AddItemsBarcodeScan extends Component {
  // const [hasPermission, setHasPermission] = useState(null);
  // const [scanned, setScanned] = useState(false);
  state = { itemData: this.props.items, getItems: [] };
  constructor(props) {
    super(props);
    this.state = {
      hasPermission: null,
      scanned: false,
      animationLineHeight: 0,
      focusLineAnimation: new Animated.Value(0),
      ItemCode: props.navigation.state.params.ItemDetails,
    };
  }

  /*Requesting for permission */
  componentDidMount = async () => {
    const { status } = await BarCodeScanner.requestPermissionsAsync();
    this.animateLine();
    // const barcode = this.props.navigation.getParams('ItemDetails');
    // console.log('Barcode item code ', barcode);
    // this.setState({ ItemCode: barcode.itemCode });
    this.setState({ hasPermission: 'granted' });
  };

  /*Handling barcode scan*/
  handleBarCodeScanned = async ({ type, data }) => {
    this.setState({ scanned: true });
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    // this.props.handleNavigation(data);
    this.props.updateItem(this.state.ItemCode, data);

    await putBarcodeId({
      barCodeId: data,
      itemCode: this.state.ItemCode,
    });

    alert(`Bar code added successfully!!!!`);

    // this.props.updateItem(data, this.state.ItemCode);
    this.props.navigation.navigate('GetItems');
    console.log('Thanks updated');
  };
  /*Animation*/
  animateLine = () => {
    Animated.sequence([
      Animated.timing(this.state.focusLineAnimation, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.focusLineAnimation, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]).start(this.animateLine);
  };

  // if (hasPermission === null) {
  //   return <Text>Requesting for camera permission</Text>;
  // }
  // if (hasPermission === false) {
  //   return <Text>No access to camera</Text>;
  // }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text
            style={{
              color: '#000000',
              textAlign: 'center',
              fontWeight: 'bold',
              margin: 10,
            }}>
            Scan Product
          </Text>
          <Text style={{ color: 'grey', textAlign: 'center', flexWrap: 'wrap' }}>Place the barcode inside the frame to scan, Please keep your device steady when scanning to ensure accurate results</Text>
        </View>
        <View
          style={{
            width: width * 0.9,
            height: height * 0.57,
            alignSelf: 'center',
            margin: '10%',
          }}>
          <BarCodeScanner onBarCodeScanned={this.state.scanned ? undefined : this.handleBarCodeScanned} style={StyleSheet.absoluteFillObject} />
          {this.state.scanned && <Button title={'Tap to Scan Again'} onPress={() => this.setState({ scanned: false })} />}
          <View style={styles.overlay}>
            <View style={styles.unfocusedContainer}></View>
            <View style={styles.middleContainer}>
              <View style={styles.unfocusedContainer}></View>
              <View
                onLayout={e =>
                  this.setState({
                    animationLineHeight: e.nativeEvent.layout.height,
                  })
                }
                style={styles.focusedContainer}>
                {!this.state.scanned && (
                  <Animated.View
                    style={[
                      styles.animationLineStyle,
                      {
                        transform: [
                          {
                            translateY: this.state.focusLineAnimation.interpolate({
                              inputRange: [0, 1],
                              outputRange: [0, this.state.animationLineHeight],
                            }),
                          },
                        ],
                      },
                    ]}
                  />
                )}
                <View style={styles.unfocusedContainer}></View>
              </View>
              <View style={styles.unfocusedContainer}></View>
            </View>
          </View>
        </View>
        {this.state.scanned && <Button title={'Tap to Scan Again'} onPress={() => this.setState({ scanned: false })} />}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  unfocusedContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  middleContainer: {
    flexDirection: 'row',
    flex: 1.5,
  },
  focusedContainer: {
    flex: 6,
  },
  animationLineStyle: {
    height: 2,
    width: '100%',
    backgroundColor: 'red',
  },
});
export default connected(AddItemsBarcodeScan);
