import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { FONTS, lightTheme } from '../../constants/Theme';

export default class GetItemsHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  HomePressed = () => {
    this.props.navigation.navigate('HomeScreen');
  };
  render() {
    return (
      <View style={styles.header}>
        {/* <Ionicons name='arrow-back-circle-outline' size={30} color={lightTheme.PRIMARY_TEXT_COLOR} style={styles.icon} onPress={this.HomePressed} /> */}
        <Ionicons name='arrow-back-circle-outline' size={32} justifyContent='space-between' alignSelf='flex-start' position='absolute' color='#CCCCCC' onPress={this.HomePressed} />
        <TextInput
          style={{ height: 24, marginTop: 0, marginLeft: 10, marginRight: 10, width: 240, borderColor: '#CCCCCC', backgroundColor: 'white', paddingLeft: 5, paddingRight: 5 }}
          // placeholder='Name/Category/SubCategory'
          placeholderColor='white'
          placeholderTextColor='#c4c3cb'
          //   onChangeText={text => onChangeText(text)}
          //   value={value}
        />
        {/* <TextInput style={styles.headerText}></Text> */}
        {/* <FontAwesome name="search-plus" size={24} color="black" /> */}
        <Ionicons name='md-search-circle-outline' size={32} justifyContent='space-between' alignSelf='flex-end' position='absolute' color='#CCCCCC' />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'space-between',
    backgroundColor: lightTheme.BLACK_COLOR,
    // justifyContent: "center",
  },
  icon: {
    alignSelf: 'flex-start',
    position: 'absolute',
  },
  loginFormTextInput: {
    height: 40,

    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: lightTheme.PRIMARY_TEXT_COLOR,
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,

    alignItems: 'stretch',
    marginTop: 5,
    marginBottom: 5,
  },
  headerText: {
    ...FONTS.h4,
    color: lightTheme.SECONDARY_TEXT_COLOR,
  },
  subheaderText: {
    ...FONTS.h3,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  icon: {
    position: 'absolute',
    right: 20,
    width: undefined,
  },
  searchicon: {
    position: 'absolute',
    right: 5,
  },
  tinyLogo: {
    width: 40,
    height: 40,
    borderRadius: 30,
  },
});
