import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { FONTS, lightTheme } from "../../constants/Theme";

export default class HomeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      state: "Karnataka",
      stateSelector: [],
      userData: {},
      userPhoto: {},
    };
  }
  componentDidMount = async () => {
    const val = await AsyncStorage.getItem("logincredential");
    const data = await AsyncStorage.getItem("loginc");
    const object = JSON.parse(data);
    const obj = JSON.parse(val);
    this.setState({ userData: obj, userPhoto: object });
  };

  render() {
    const { userData, userPhoto } = this.state;
    return (
      <View style={styles.header}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flexDirection: "column" }}>
            <Text style={styles.headerText}>Hello</Text>
            <Text style={styles.subheaderText}>
              {userData?.firstName || ""}
            </Text>
          </View>
          {/* <Image style={styles.tinyLogo} source={require('../../../assets/images/handWave.png')} /> */}
        </View>

        <Image style={styles.tinyLogo} source={{ uri: userPhoto.photoUrl }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    color: lightTheme.BLACK_COLOR,
    // justifyContent: "center",
  },
  headerText: {
    ...FONTS.h4,
    color: lightTheme.SECONDARY_TEXT_COLOR,
  },
  subheaderText: {
    ...FONTS.h3,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  icon: {
    position: "absolute",
    right: 20,
    width: undefined,
  },
  searchicon: {
    position: "absolute",
    right: 5,
  },
  tinyLogo: {
    width: 40,
    height: 40,
    borderRadius: 30,
  },
});
