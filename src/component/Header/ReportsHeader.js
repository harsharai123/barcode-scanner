import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { FONTS } from '../../constants';
import { lightTheme } from '../../constants/Theme';

export default class ReportsHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.roundButton1}>
          <Text> Reports</Text>
        </TouchableOpacity>
        <Text style={styles.headerText}>Reports</Text>
        {/* <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={styles.headerText}>Hello</Text>
            {/* <Text style={styles.subheaderText}>{userData.givenName}</Text> */}

        {/* </View>
          <Image style={styles.tinyLogo} source={require('../../../assets/images/handWave.png')} />
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between',
    backgroundColor: lightTheme.WHITE_COLOR,
    // justifyContent: "center",
  },
  roundButton1: {
    width: 50,
    height: 50,

    padding: 10,
    borderRadius: 100,
    backgroundColor: 'orange',
  },
  headerText: {
    ...FONTS.h2,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  // headerText: {
  //   ...FONTS.h1,
  //   color: lightTheme.PRIMARY_TEXT_COLOR,
  // },
  // subheaderText: {
  //   ...FONTS.h4,
  //   color: lightTheme.SECONDARY_TEXT_COLOR,
  // },
  // icon: {
  //   position: 'absolute',
  //   right: 20,
  //   width: undefined,
  // },
  // searchicon: {
  //   position: 'absolute',
  //   right: 5,
  // },
  button: {
    width: 40,
    height: 40,
    alignSelf: 'flex-end',
    position: 'absolute',
    borderRadius: 5,
    margin: 5,
  },
});
