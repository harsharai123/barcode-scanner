import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import { FONTS } from '../../constants';
import { lightTheme } from '../../constants/Theme';

export default class CreateBarcodeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  CreatePressed = () => {
    Alert.alert('ALERT', 'Please download or print the barcode before you leave this page', [
      {
        text: 'okay',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Leave without printing',
        onPress: () => {
          this.props.navigation.navigate('GetItems');
        },
      },
    ]);
  };
  render() {
    return (
      <View style={styles.header}>
        <Ionicons name='arrow-back-circle-outline' size={30} color={lightTheme.PRIMARY_TEXT_COLOR} style={styles.icon} onPress={this.CreatePressed} />
        <Text style={styles.headerText}>Create Barcode</Text>
        {/* <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={styles.headerText}>Hello</Text>
            {/* <Text style={styles.subheaderText}>{userData.givenName}</Text> */}

        {/* </View>
          <Image style={styles.tinyLogo} source={require('../../../assets/images/handWave.png')} />
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between',
    backgroundColor: lightTheme.BLACK_COLOR,

    // justifyContent: "center",
  },
  headerText: {
    ...FONTS.h2,
    marginLeft: 10,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  // headerText: {
  //   ...FONTS.h1,
  //   color: lightTheme.PRIMARY_TEXT_COLOR,
  // },
  // subheaderText: {
  //   ...FONTS.h4,
  //   color: lightTheme.SECONDARY_TEXT_COLOR,
  // },
  // icon: {
  //   position: 'absolute',
  //   right: 20,
  //   width: undefined,
  // },
  // searchicon: {
  //   position: 'absolute',
  //   right: 5,
  // },
  button: {
    width: 40,
    height: 40,
    alignSelf: 'flex-end',
    position: 'absolute',
    borderRadius: 5,
    margin: 5,
  },
});
