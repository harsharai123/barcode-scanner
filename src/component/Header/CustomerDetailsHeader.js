import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FONTS, lightTheme } from '../../constants';

export default class CustomerDetailsHeader extends Component {
  constructor(props) {
    super(props);
  }
  cartPressed = () => {
    this.props.navigation.navigate('BillScreen');
  };
  componentDidMount = async () => {
    console.log('dddssdsd');

    // db.transaction((tx) => {
    //   tx.executeSql("SELECT * FROM table_user", [], (tx, results) => {
    //     console.log("results", results);
    //     var temp = [];
    //     const lastItem = results.rows._array[results.rows._array.length - 1];
    //     console.log("userdetails", lastItem);
    //     this.setState({ userDetail: lastItem });
    //     this.setState({ userImage: lastItem.user_image });
    //   });
    // });
  };

  render() {
    return (
      <View style={styles.header}>
        <Ionicons name='arrow-back-circle-outline' size={30} color={lightTheme.PRIMARY_TEXT_COLOR} style={styles.icon} onPress={this.cartPressed} />
        <Text style={styles.headerText}>Customer Details</Text>

        {/* <Ionicons name={Platform.OS === 'android' ? 'md-search' : 'ios-menu'} size={24} color='black' style={styles.searchicon} /> */}
        {/* <Ionicons name={'cart'} size={24} color='black' style={styles.carticon} /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: lightTheme.BLACK_COLOR,
    // justifyContent: "center",
  },
  headerText: {
    ...FONTS.h2,
    marginLeft: 10,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  carticon: {
    position: 'absolute',
    right: 5,
  },
  searchicon: {
    position: 'absolute',
    right: 50,
  },
});
