import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FONTS, lightTheme } from '../../constants';

export default class CartHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userDetail: '',
    };
  }
  componentDidMount = async () => {
    console.log('dddssdsd');

    // db.transaction((tx) => {
    //   tx.executeSql("SELECT * FROM table_user", [], (tx, results) => {
    //     console.log("results", results);
    //     var temp = [];
    //     const lastItem = results.rows._array[results.rows._array.length - 1];
    //     console.log("userdetails", lastItem);
    //     this.setState({ userDetail: lastItem });
    //     this.setState({ userImage: lastItem.user_image });
    //   });
    // });
  };
  HomePressed = () => {
    this.props.navigation.navigate('HomeScreen');
  };
  render() {
    return (
      <View style={styles.header}>
        <Ionicons name='arrow-back-circle-outline' size={30} color={lightTheme.PRIMARY_TEXT_COLOR} style={styles.icon} onPress={this.HomePressed} />
        <Text style={styles.headerText}>Cart</Text>

        {/* <Ionicons name={'md-search-circle-outline'} size={32} color='black' style={styles.searchicon} /> */}
        <Ionicons name={'cart'} size={30} color='black' style={styles.carticon} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: "center",
  },
  icon: {
    alignSelf: 'flex-start',
  },
  headerText: {
    marginLeft: 10,
    ...FONTS.h2,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  carticon: {
    position: 'absolute',
    color: lightTheme.PRIMARY_TEXT_COLOR,
    right: 5,
  },
  searchicon: {
    position: 'absolute',
    color: lightTheme.PRIMARY_TEXT_COLOR,
    right: 50,
  },
});
