import { Feather, Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, View } from 'react-native';
import { FONTS } from '../../constants';
import { lightTheme } from '../../constants/Theme';

export default class ProfileHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  homeScreenPressed = () => {
    this.props.navigation.navigate('HomeScreen');
  };
  render() {
    return (
      <View style={styles.header}>
        <StatusBar color='#fff' />
        {/* <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={styles.headerText}>Hello</Text>
            {/* <Text style={styles.subheaderText}>{userData.givenName}</Text> */}

        <Ionicons name='arrow-back-circle-outline' size={30} color={lightTheme.PRIMARY_TEXT_COLOR} onPress={this.homeScreenPressed} />

        <Text style={styles.headerText}>Profile </Text>

        <Feather name='edit' size={26} justifyContent='space-between' alignSelf='flex-end' position='absolute' color={lightTheme.PRIMARY_BACKGROUND_COLOR} onPress={() => this.props.navigation.navigate('EditProfile')} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    // alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  roundButton1: {
    width: 34,
    height: 34,
    borderWidth: 2,
    borderColor: lightTheme.PRIMARY_TEXT_COLOR,
    padding: 10,

    borderRadius: 17,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  headerText: {
    ...FONTS.h2,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  // headerText: {
  //   ...FONTS.h1,
  //   color: lightTheme.PRIMARY_TEXT_COLOR,
  // },
  // subheaderText: {
  //   ...FONTS.h4,
  //   color: lightTheme.SECONDARY_TEXT_COLOR,
  // },
  // icon: {
  //   position: 'absolute',
  //   right: 20,
  //   width: undefined,
  // },
  // searchicon: {
  //   position: 'absolute',
  //   right: 5,
  // },
  button: {
    width: 40,
    height: 40,
    alignSelf: 'flex-end',
    position: 'absolute',
    borderRadius: 5,
    margin: 5,
  },
});
