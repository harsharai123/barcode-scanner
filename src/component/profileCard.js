import PropTypes from 'prop-types';
import React from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import { FONTS, lightTheme } from '../constants';
import styles from '../container/styles/profileScreen';
const { height, width } = Dimensions.get('window');

const AboutusCard = props => {
  return (
    <View
      style={{
        ...styles.cardContainer,
        ...props.cardContainer,
        // paddingVertical: 10,
      }}>
      <View
        style={{
          flex: 3.5,
          backgroundColor: lightTheme.SECONDARY_COLOR,
          borderTopLeftRadius: 30,
          borderTopRightRadius: 30,
        }}></View>
      <View
        style={{
          flexGrow: 2,
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
          backgroundColor: lightTheme.TERNARY_COLOR,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image style={{ ...styles.FacultyStyles, ...props.FacultyStyles }} source={props.facultyImage} />
        <View style={{ marginVertical: 20 }}>
          <Text
            style={{
              ...FONTS.h3,
              color: 'orange',
              textAlign: 'center',
            }}>
            {props.userName}
          </Text>
          <Text
            style={{
              marginVertical: 5,
              ...FONTS.body3,
              color: lightTheme.PRIMARY_TEXT_COLOR,
              textAlign: 'center',
            }}>
            {props.userStore}
          </Text>
          <View
            style={{
              borderStartWidth: 1,
              borderRightWidth: 1,
              borderTopWidth: 1,
              borderColor: lightTheme.PRIMARY_TEXT_COLOR,
              borderWidth: 1,
              marginVertical: 15,
            }}
          />
          <Text
            style={{
              marginVertical: 5,
              ...FONTS.h4,
              color: lightTheme.PRIMARY_TEXT_COLOR,
              textAlign: 'center',
            }}>
            {props.emailId}
          </Text>
          <Text
            style={{
              marginVertical: 5,
              ...FONTS.h4,
              color: lightTheme.PRIMARY_TEXT_COLOR,
              textAlign: 'center',
            }}>
            {props.mobileNumber}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default AboutusCard;

AboutusCard.propTypes = {
  cardText: PropTypes.string,
};

AboutusCard.defaultProps = {
  cardText: 'Home',
  trusteeDetails: false,
  userStore: 'SHREE GENERAL STORE',
  userName: 'Vasanth Kumar',
  mobileNumber: '9632454545',
  emailId: 'admin@gmail.com',
  facultyImage: require('../../assets/images/avatar.png'),
  websiteaddress: 'Aikya House, Behind Mayura theatre, Puttur',
};
