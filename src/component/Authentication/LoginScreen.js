import AsyncStorage from "@react-native-async-storage/async-storage";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import { Component, default as React } from "react";
import { Alert, Image, StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import BarcodeLogo from "../../../assets/BarcodeLogo.png";
import Lock from "../../../assets/images/lock.png";
import { addUser, getItemsApi, getUserDetails } from "../../../NewServices";
import {
  _storeData,
  _removeData,
  _retrieveData,
} from "../../utils/storage_utils";
import { FONTS, lightTheme } from "../../constants";
import { ITEMS_LIST } from "../../constants/ConfigConstants";
import CustomButton from "../../container/CustomisedButtons/CustomButton";
import FacebookButton from "../../container/CustomisedButtons/FacebookButton";
import { getItems } from "../../store/actions/itemActions";
import { updateUserDetails } from "../../store/actions/userActions";
import FullScreenLoader from "../FullScreenLoader";
const isNeedErrorPopup = true;
const errPopup = (err) =>
  isNeedErrorPopup &&
  Alert.alert("Error", err.message, [
    {
      text: "Cancel",
      onPress: () => console.log("Cancel Pressed"),
      style: "cancel",
    },
  ]);

class LoginScreen extends Component {
  state = { pageLoader: false, authLoader: false };
  async componentDidMount() {
    await this.setState({ pageLoader: true });
    try {
      const val = await AsyncStorage.getItem("logincredential");
      const itemsList = await AsyncStorage.getItem(ITEMS_LIST);
      const obj = JSON.parse(val);
      const itemData = JSON.parse(itemsList);
      this.props.getItems(itemData);
      if (Obj) {
        console.log(Obj);
        setTimeout(() => {
          this.setState({ pageLoader: false });
          this.props.navigation.navigate("HomeScreen");
        }, 2000);
      } else await this.setState({ pageLoader: false });
    } catch (error) {
      await this.setState({ pageLoader: false });
    }
  }

  signInWithFBAsync = async () => {
    try {
      await Facebook.initializeAsync({ appId: "<APP_ID>" });
      const { type, token, expirationDate, permissions, declinedPermissions } =
        await Facebook.logInWithReadPermissionsAsync({
          permissions: ["public_profile"],
        });
      if (type === "success") {
        const response = await fetch(
          `https://graph.facebook.com/me?access_token=${token}`
        ).then((resp) => resp.json());
        // await addUser({
        //   createdOn: '2021-05-28T16:38:24',
        //   email: email,
        //   firstName: name,
        //   id: '2',
        //   lastName: familyName,
        //   loginType: 'Google',
        //   mobileNumber: ' 9099999999',
        //   userName: givenName,
        // }).catch(err => {
        //   console.log('Error fetching data');
        //   errPopup(err);
        // });
        await AsyncStorage.setItem("logincredential", JSON.stringify(response));

        this.props.navigation.navigate("HomeScreen");
      } else return { cancelled: true };
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  };

  setUserDataandStore = async (userDetails, userData) => {
    let userDatas = { ...userDetails };
    userDatas.photoUrl = userData.photoUrl;
    console.log("USERDATAINSTORINGNEW", userDatas);
    await _storeData("logincredential", JSON.stringify(userDatas));
    await getItemsApi()
      .then((item) => {
        // console.log('Items', item);
        this.props.getItems(item);
      })
      .catch((err) => {});
    this.setState({ authLoader: false });
    this.props.navigation.navigate("HomeScreen");
  };

  /*Add user details in server*/

  signInWithGoogleAsync = async () => {
    this.setState({ authLoader: true });
    try {
      const result = await Google.logInAsync({
        androidClientId:
          "139969375953-8ugngeafh9012tlj7fd2kdo2mldinicl.apps.googleusercontent.com",
        // androidClientId: '139969375953-mog1412bqhhhaq023ss15ajtnmipbvod.apps.googleusercontent.com',
        // androidStandaloneAppClientId: '139969375953-8ugngeafh9012tlj7fd2kdo2mldinicl.apps.googleusercontent.com',
        iosClientId:
          "139969375953-kg3cnbradb8cpepv3a0dvgcjcq4gg2c7.apps.googleusercontent.com",
        scopes: ["profile", "email"],
      });
      if (result.type === "success") {
        console.log("CREDENTIALS", result);
        const { email, familyName, givenName, id, name, photoUrl } =
          result.user;
        const user = {
          email,
          name,
          givenName,
          photoUrl,
          familyName,
          id,
          loginType: "Google",
        };
        console.log("USerDATASS", user);
        let userData = { ...user };
        console.log("USerDATASS", userData);
        //  await AsyncStorage.setItem("loginc", JSON.stringify(user));
        await _storeData("loginc", JSON.stringify(user));
        console.log("userr", user);
        await addUser({
          createdOn: "2021-05-28T16:38:24",
          email: email,
          firstName: name,
          id: "2",
          lastName: familyName,
          loginType: "Google",
          mobileNumber: " 9099999999",
          userName: givenName,
          loginId: id,
        })
          .then(async (resp) => {
            console.log("ID from google  fetched", resp);
            // await AsyncStorage.setItem('loginc', JSON.stringify(user));
            console.log("userr", user);
            await getUserDetails({
              loginId: id,
              loginType: "Google",
            })
              .then(async (userDetails) => {
                // console.log('userr', user);
                console.log("CHECKINGSCHOOLDATA", userDetails);
                if (userDetails && userDetails !== undefined) {
                  this.setUserDataandStore(userDetails, userData);
                } else {
                  console.log("APP STOPP HERE");
                  this.setState({
                    authLoader: false,
                    userAuthenticationFail: true,
                  });
                  //  await _storeData("logincredential", JSON.stringify(userData));
                }

                // await AsyncStorage.setItem(
                //   "logincredential",
                //   JSON.stringify(user)
                //          );
                // this.props.updateUserDetails(user);
              })
              .catch((err) => {
                console.log("ERRR", err);
              });
          })
          .catch((err) => {
            console.log("Error fetching data");
            errPopup(err);
            this.setState({ authLoader: false });
          });
      } else return { cancelled: true };
    } catch (e) {
      console.log(e);
      this.setState({ authLoader: false });
      errPopup(e);
      return { error: true };
    }
  };

  render() {
    const { pageLoader, authLoader } = this.state;

    return (
      <View style={styles.image}>
        {!this.state.userAuthenticationFail ? (
          <View>
            <View style={styles.logoContainer}>
              <Image source={BarcodeLogo} style={styles.logo} />
            </View>
            <View style={styles.lockContainer}>
              <Image source={Lock} style={styles.lock} />
              <Text style={styles.userLoginText}>USER LOGIN</Text>
            </View>
            <View style={styles.PageLoader}>
              {pageLoader ? (
                <View>
                  <FullScreenLoader />
                </View>
              ) : (
                <View style={styles.LoginButton}>
                  {authLoader && <FullScreenLoader />}
                  <CustomButton
                    title="Google Sign in"
                    onPress={this.signInWithGoogleAsync}
                  />
                  <FacebookButton
                    title="Facebook Sign in"
                    onPress={this.signInWithFBAsync}
                  />
                </View>
              )}
            </View>
          </View>
        ) : (
          <View>
            <Text>Result</Text>
          </View>
        )}
        {/* <Text style={styles.text}>Barcode Scanner</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 250,
    height: 159,
  },
  userLoginText: {
    marginTop: 10,
    ...FONTS.h3,

    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  lockContainer: {
    marginTop: 100,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  lock: {
    width: 60,
    height: 60,
  },
  logoContainer: {
    marginTop: 20,
    // marginLeft: 70,
    alignItems: "center",
    justifyContent: "center",
  },
  PageLoader: {
    // marginTop: 50,
    width: "100%",
    height: "20%",
    alignItems: "center",
    justifyContent: "center",
  },
  LoginButton: {
    marginRight: 20,
    marginLeft: 20,
    alignItems: "center",
    // justifyContent: 'center',
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    backgroundColor: "#282828",
  },
  text: {
    marginTop: 80,
    color: "yellow",
    marginLeft: 20,
    ...FONTS.h1,
    fontWeight: "bold",
  },
});
const mapStateToProps = (state) => ({});
const mapDispatchToProps = { getItems, updateUserDetails };

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
