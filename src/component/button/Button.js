import React from "react";
import { TouchableOpacity, Text, View, Image } from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";
const Button = (props) => {
  const { buttonStyles, buttonText, stylebuttonText } = props;
  return (
    <TouchableOpacity
      style={{ ...styles.buttonContainer, ...buttonStyles }}
      onPress={props.onPressButton}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View style={{ justifyContent: "center" }}>
          {props.showImage ? (
            <Image
              style={{ ...styles.socialIcons, ...props.socialiconsStyle }}
              source={props.socialicons}
            />
          ) : null}
        </View>
        <View
          style={{
            justifyContent: "center",
            alignContent: "center",
            alignSelf: "center",
          }}
        >
          <Text style={{ ...styles.buttonText, ...stylebuttonText }}>
            {buttonText}
          </Text>
        </View>
        <View></View>
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  buttonText: PropTypes.string,
  showImage: PropTypes.bool,
};

Button.defaultProps = {
  buttonText: "Sign In",
  showImage: true,
};

export default Button;
