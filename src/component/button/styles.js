export default {
  buttonContainer: {
    width: '100%',
    padding: 15,
    justifyContent: 'center',
    alignItem: 'center',
    borderRadius: 30,
    backgroundColor: '#B5DFED',
  },
  buttonText: {
    textAlign: 'center',
  },
  socialIcons: {
    width: 25,
    height: 25,
  },
};
