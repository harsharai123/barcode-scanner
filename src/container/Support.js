import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import PrivacyPolicyHeader from '../component/Header/PrivacyPolicyHeader';
import { FONTS } from '../constants';

class PrivacyPolicy extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <PrivacyPolicyHeader navigation={navigation} />,
    };
  };
  agreePressed = () => {
    this.props.navigation.navigate('ProfileScreen');
  };
  render() {
    return (
      <View style={styles.fullScreen}>
        <ScrollView>
          <Text style={{ margin: 10, ...FONTS.h5 }}> Please Contact contact@roupya.com</Text>
        </ScrollView>
        {/* <TouchableHighlight style={{ shadowColor: lightTheme.WHITE_COLOR, shadowOpacity: 0.26, shadowOffset: { width: 0, height: 2 }, shadowRadius: 8, elevation: 5, width: '40%', height: '10%', padding: 20, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', backgroundColor: lightTheme.SECONDARY_COLOR }} onPress={this.agreePressed}>
          <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>Agree</Text>
        </TouchableHighlight> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  fullScreen: {
    margin: 10,
  },
});
export default PrivacyPolicy;
