import React, { Component } from 'react';
import { Alert, BackHandler, SafeAreaView, View } from 'react-native';
import { connect } from 'react-redux';
import BarCodeScan from '../component/scanner/BarCodeScan';
import { addProduct, updateCartQuantity } from '../store/actions/cartActions';
import styles from './styles/HomeScreen';
const mapStateToProps = state => ({ cart: state.cart.cart, subtotal: state.cart.subtotal, tax: state.cart.tax, items: state.items });
const connected = comp => connect(mapStateToProps, { addProduct, updateCartQuantity })(comp);

class Scanner extends Component {
  state = { barCodeScannerVisible: true };

  handleBackPress = () => {
    Alert.alert(
      'Do you want to Exit the App?',
      ' ',
      [
        { text: 'No', style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false }
    );
  };

  scanned = val => {
    let item = this.props.items.find(ele => ele.barCodeId === val);
    if (item) {
      const index = this.props.cart.findIndex(ele => ele.barCodeId === item.barCodeId);
      if (index === -1) {
        item.quantity = 1;
        item.mrp = parseInt(item.mrp) || 0;
        item.gst = parseInt(item.gst) || 0;
        item.taxprice = (item.mrp / 100) * item.gst;
        this.props.addProduct(item);
      } else {
        item = this.props.cart[index];
        this.props.updateCartQuantity(item.barCodeId, item.quantity + 1);
      }
      this.setState({ barCodeScannerVisible: false });
      this.props.navigation.navigate('BillScreen', { barCodeIdID: item.barCodeId });
    } else Alert.alert('Scanned Item doesnt exist');
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1 }}>
          <BarCodeScan handleNavigation={this.scanned} />
        </View>
      </SafeAreaView>
    );
  }
}

export default connected(Scanner);
