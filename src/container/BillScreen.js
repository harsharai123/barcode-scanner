import React, { Component } from 'react';
import { Image, SafeAreaView, Text, TouchableHighlight, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import BillCard from '../component/billCard/BillCard';
import CartHeader from '../component/Header/CartHeader';
import { FONTS, lightTheme } from '../constants';
import { addProduct, removeFromCart, totalPrice, updateCartQuantity, updateItemPrice, updateTax } from '../store/actions/cartActions';
import styles from './styles/BillScreen';
class BillScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      addsmodalVisible: false,
      dataValues: [],
      value: '',
      showPost: false,
      search: '',
      isLoading: true,
      page: 0,
      isFetching: false,
      isVisible: false,
      selectedProducts: this.props.cart || [],
      subtotal: this.props.subtotal,
      totalPrice: 0,
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <CartHeader navigation={navigation} />,
    };
  };

  /*Fetches the value from the store */
  componentDidMount = async () => {
    console.log('Dssssfdd', this.props.cart);
    this.props.updateItemPrice(this.state.selectedProducts);
    // this.setState({ selectedProducts: this.props.cart });
    // this.setState({ data: Products });
    console.log('this.props.total ', totalEstimatedAmountIncludingGst);
    this.setState({ totalPrice: totalEstimatedAmountIncludingGst });
  };

  componentDidUpdate = () => {
    this.props.totalPrice(this.props.subtotal, this.props.tax);
    console.log('1calling ', this.props.total);
    // this.setState({ totalPrice: this.props.total });
  };

  UNSAFE_componentWillReceiveProps = async () => {
    console.log('onchangeProps', this.props.cart);
    this.setState({ selectedProducts: this.props.cart });
    console.log('DDbD', this.props.subtotal, this.props.tax);
    this.setState({ totalPrice: this.props.total });
  };

  /*Editing the value of bill Card price*/
  changebillcardValue = (productPrice, product) => {
    console.log('Values', productPrice, product);
    let ProdPrice = JSON.parse(productPrice);
    product.Price = ProdPrice;
    this.props.removeFromCart(product.id);
    this.props.addProduct(product);
    this.props.updateTax(this.props.cart);
    console.log('this.props.cart', this.props.cart);
    this.props.updateItemPrice(this.props.cart);
    this.props.totalPrice(this.props.subtotal, this.props.tax);
  };

  //   componentWillUnmount() {
  //     clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
  //     this._didFocusSubscription && this._didFocusSubscription.remove();
  //     this._willBlurSubscription && this._willBlurSubscription.remove();
  //   }

  render() {
    const { selectedProducts } = this.state;
    console.log('this.state.selectedProducts', this.state.selectedProducts);
    const totalEstimatedAmountExcludingGst = selectedProducts.reduce((t, c) => t + c.totalEstimatedAmountExcludingGst, 0);
    const totalEstimatedAmountIncludingGst = selectedProducts.reduce((t, c) => t + c.totalEstimatedAmountIncludingGst, 0);
    console.log('totalEstimatedAmountIncludingGst', totalEstimatedAmountIncludingGst);
    // const { totalEstimatedAmountIncludingGst } = this.state;
    if (this.state.selectedProducts.length != 0) {
      return (
        <SafeAreaView style={styles.container}>
          <View style={{ flex: 2 }}>
            <ScrollView>
              <BillCard selectedItems={this.state.selectedProducts} changebillcardValue={(value, item) => this.changebillcardValue(value, item)} />
            </ScrollView>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ marginLeft: 15, marginBottom: 15, marginTop: 20 }}>
              <Text style={{ ...FONTS.h4 }}>Estimation Summary</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft: 15,
                marginRight: 15,
                marginBottom: 5,
              }}>
              <Text style={{ ...FONTS.h5 }}>Total Estimated Amount Excluding GST</Text>
              <Text style={{ ...FONTS.h5 }}>₹ {totalEstimatedAmountExcludingGst}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft: 15,
                marginRight: 15,
                marginBottom: 5,
              }}>
              <Text style={{ ...FONTS.h5 }}>Total Estimated Amount Including GST</Text>
              <Text style={{ ...FONTS.h5 }}>₹ {totalEstimatedAmountIncludingGst}</Text>
            </View>
            {/* <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft: 15,
                marginRight: 15,
                marginBottom: 5,
              }}>
              <Text style={{ ...FONTS.h5 }}>Delivery</Text>
              <Text style={{ ...FONTS.h5, color: lightTheme.BLACK_COLOR }}>Free</Text>
            </View> */}

            <View
              style={{
                marginLeft: 15,
                marginRight: 15,
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <View
              style={{
                flexDirection: 'row',
                margin: 15,
                justifyContent: 'space-between',
              }}>
              {/* <View style={{ flexDirection: 'column' }}>
                <Text style={{ ...FONTS.body2 }}>Total</Text>
                <Text style={{ ...FONTS.h4, marginTop: 10 }}>₹ {this.props.total}</Text>
              </View> */}
              <View style={{ marginLeft: 110 }}>
                <TouchableHighlight
                  style={{
                    marginTop: 10,
                    shadowColor: lightTheme.WHITE_COLOR,
                    shadowOpacity: 0.26,
                    shadowOffset: { width: 0, height: 2 },
                    shadowRadius: 8,
                    elevation: 5,
                    width: '100%',
                    height: '10%',
                    padding: 20,
                    borderRadius: 10,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    backgroundColor: lightTheme.SECONDARY_COLOR,
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('CustomerDetails', { total: totalEstimatedAmountIncludingGst });
                  }}>
                  <Text
                    style={{
                      ...FONTS.h5,
                      textAlign: 'center',
                      color: lightTheme.WHITE_COLOR,
                    }}>
                    Check Out
                  </Text>
                </TouchableHighlight>
              </View>
              {/* <Invoice /> */}
              {/* <Button
                buttonStyles={{
                  ...styles.buttonStyles,
                  backgroundColor: lightTheme.SECONDARY_COLOR,
                }}
                showImage={false}
                stylebuttonText={styles.buttonText}
                buttonText="Send Invoice"
                onPressButton={() => this.execute(this.state.selectedProducts)}
              /> */}
            </View>
          </View>
        </SafeAreaView>
      );
    } else {
      return (
        <View style={styles.container}>
          <Image source={require('../../assets/images/empty_basket.jpg')} style={{ resizeMode: 'cover', width: '100%', height: '70%' }} />
          <Text
            style={{
              ...FONTS.h4,
              textAlign: 'center',
              color: lightTheme.BLACK_COLOR,
            }}>
            Whoops...Nothing is here!
          </Text>
          <Text
            style={{
              ...FONTS.body2,
              textAlign: 'center',
              color: lightTheme.BLACK_COLOR,
            }}>
            Explore around to add items to your cart
          </Text>
        </View>
      );
    }
  }
}

const mapStateToProps = state => ({
  cart: state.cart.cart,
  subtotal: state.cart.subtotal,
  tax: state.cart.tax,
  total: state.cart.total,
});

const mapDispatchToProps = {
  updateCartQuantity,
  updateItemPrice,
  totalPrice,
  removeFromCart,
  updateTax,
  addProduct,
};
export default connect(mapStateToProps, mapDispatchToProps)(BillScreen);
