import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import {
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from "react-native";
import { Text, TouchableRipple } from "react-native-paper";
import { connect } from "react-redux";
import ProfileHeader from "../component/Header/ProfileHeader";
import AboutusCard from "../component/profileCard";
import { FONTS, lightTheme } from "../constants";
import { clearCart } from "../store/actions/cartActions";
import { _removeData } from "../utils/storage_utils";
const connected = (comp) => connect((state) => ({}), { clearCart })(comp);
class ProfileScreen extends Component {
  updateUserDetails = async () => {
    const val = await AsyncStorage.getItem("logincredential");
    const obj = JSON.parse(val);
    this.setState({ userData: obj });
  };
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => (
        <ProfileHeader
          navigation={navigation}
          onUpdateUserDetails={() => this.updateUserDetails()}
        />
      ),
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      userPhoto: {},
    };
  }
  componentDidMount = async () => {
    const data = await AsyncStorage.getItem("loginc");
    const object = JSON.parse(data);
    console.log("RESULTANTDATA", object);
    this.setState({ userPhoto: object });
    this.updateUserDetails();
    this.props.navigation.setParams({
      onUpdateUserDetails: () => this.updateUserDetails(),
    });
  };

  logout = async () => {
    console.log(this.props.navigation);
    _removeData("loginc");
    AsyncStorage.setItem("logincredential", "");
    this.props.clearCart();
    this.props.navigation.navigate("Login");
  };
  createTwoButtonAlert = () =>
    Alert.alert("Log Out", "Are you Sure??", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          this.logout();
        },
      },
    ]);

  render() {
    const { userData, userPhoto } = this.state;
    console.log("USERDATA", userData);
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar />
        <ScrollView>
          {/* <Text
            style={{
              ...FONTS.h2,
              color: lightTheme.PRIMARY_TEXT_COLOR,
              textAlign: 'center',
              marginVertical: 10,
            }}>
            PROFILE INFO
          </Text> */}
          {/* <View style={{ position: 'absolute', top: 35, right: 10 }}>
          <Image
            style={{
              width: 30,
              height: 30,
            }}
            resizeMode='contain'
            source={require('../../assets/images/avatar.png')}
          />
        </View> */}

          <View style={styles.header}>
            <AboutusCard
              userName={userData.userName}
              facultyImage={{ uri: userPhoto.photoUrl }}
              userStore="SHREE STORE"
              emailId={userData.email}
              mobileNumber={userData.mobileNumber}
            />
          </View>
          <View style={styles.menuWrapper}>
            <TouchableRipple onPress={() => {}}>
              <View style={styles.menuItem}>
                <Text
                  style={styles.menuItemText}
                  onPress={() =>
                    this.props.navigation.navigate("PrivacyPolicy")
                  }
                >
                  Privacy Policy
                </Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={() => {}}>
              <View style={styles.menuItem}>
                <Text style={styles.menuItemText}>Support</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={() => this.createTwoButtonAlert()}>
              <View style={styles.menuItem}>
                <Text style={styles.menuItemText}>Log Out</Text>
              </View>
            </TouchableRipple>
            {/* <TouchableRipple onPress={() => this.createTwoButtonAlert()}>
            <View style={styles.LogOutmenuItem}>
              <AntDesign name="logout" color="#FF6347" size={20} />
              <Text style={styles.LogoutText}>Log Out </Text>
            </View>
          </TouchableRipple> */}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default connected(ProfileScreen);

const styles = StyleSheet.create({
  header: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
    paddingVertical: 20,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  image: {
    resizeMode: "cover",
    justifyContent: "center",
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
  },
  title: {
    // fontSize: 24,
    fontWeight: "bold",
    ...FONTS.h2,
  },
  caption: {
    // fontSize: 14,
    lineHeight: 14,
    // fontWeight: '500',
    color: "black",
    ...FONTS.h5,
  },
  row: {
    flexDirection: "row",
    marginBottom: 16,
  },
  infoBoxWrapper: {
    borderBottomColor: "#777777",
    borderBottomWidth: 1,
    borderTopColor: "#777777",
    borderTopWidth: 1,
    flexDirection: "row",
    height: 100,
  },
  infoBox: {
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  menuWrapper: {
    marginTop: 5,
  },
  menuItem: {
    flexDirection: "row",
    paddingVertical: 15,

    borderBottomColor: "#777777",
    width: "50%",
    justifyContent: "center",
    alignSelf: "center",
    borderBottomWidth: 0.5,
    ...FONTS.h4,
  },
  LogOutmenuItem: {
    flexDirection: "row",
    paddingVertical: 15,
    borderBottomColor: "#777777",
    borderBottomWidth: 0.5,
    borderTopColor: "#777777",
    borderTopWidth: 0.5,
    ...FONTS.h2,
  },
  menuItemText: {
    color: lightTheme.PRIMARY_TEXT_COLOR,
    ...FONTS.body5,
    fontWeight: "600",
    fontSize: 16,
    marginLeft: 20,
    lineHeight: 26,
    textAlign: "center",
  },
  LogoutText: {
    color: "red",
    marginLeft: 24,
    fontWeight: "600",
    fontSize: 16,
    lineHeight: 26,
    ...FONTS.h4,
  },
});
