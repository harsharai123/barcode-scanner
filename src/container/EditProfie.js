import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import {
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { updateUser } from "../../NewServices";
import EditProfileHeader from "../component/Header/EditProfileHeader";
import { FONTS, lightTheme } from "../constants";
class EditProfile extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <EditProfileHeader navigation={navigation} />,
    };
  };
  state = {
    userData: {},
    customerusername: "",
    customername: "",
    customeremail: "",
    customerphone: "",
    customeremail: "",
  };

  handleUserName = (text) => {
    this.setState({ givenName: text });
  };
  handleFirstName = (text) => {
    this.setState({ name: text });
  };
  handleLastName = (text) => {
    this.setState({ familyName: text });
  };
  handlePhone = (text) => {
    this.setState({ customerphone: text });
  };

  Submit = (username, firstname, lastname, phone) => {
    alert(
      "Customerusername: " +
        username +
        "Customerfirstname: " +
        firstname +
        " Customerlastname: " +
        lastname +
        " Customerphone: " +
        phone
    );
  };

  saveChangesPressed = async () => {
    const {
      customerusername,
      customerfirstname,
      customerlastname,
      customerphone,
      userData,
    } = this.state;
    const userFinalData = {
      firstName: this.state.givenName,
      lastName: this.state.familyName,
      userName: this.state.name,
      mobileNumber: this.state.customerphone,
      email: userData.email,
      loginId: userData.loginId,
      loginType: userData.loginType,
    };
    updateUser(userFinalData)
      // { billGeneratedDate: '2021-05-28T16:38:24', billAmount: this.props.total, userPhone: this.props.data.customerphonebillGeneratedDate: '2021-05-28T16:38:24', billAmount: this.props.total, userPhone: this, userName: this.props.data.customername, invoiceUrl: '53', InvoiceId: '' })
      .then(async (response) => {
        const val = await AsyncStorage.getItem("logincredential");
        console.log("async", val);
        // console.log('dataaa', { ...userData, ...userFinalData });
        const userData = JSON.parse(val);
        await AsyncStorage.setItem(
          "logincredential",
          JSON.stringify({ ...userData, ...userFinalData })
        );
        this.props.navigation.state.params.onUpdateUserDetails();
        console.log("dataaa", { ...userData, ...userFinalData });
        this.props.navigation.navigate("ProfileScreen");

        console.log(response);
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  componentDidMount = async () => {
    const val = await AsyncStorage.getItem("logincredential");
    const userDetails = JSON.parse(val);
    this.setState({ userData: userDetails });
    this.setState({ givenName: userDetails.userName });
    this.setState({ name: userDetails.firstName });
    this.setState({ familyName: userDetails.lastName });
    this.setState({ customerphone: userDetails.mobileNumber });
    console.log("updateddd", this.props.navigation.state.params);
  };

  render() {
    const {
      customerusername,
      customerfirstname,
      customerlastname,
      customerphone,
      userData,
    } = this.state;

    // const user = { customerusername, customerfirstname, customerlastname, customerphone };

    return (
      // <View style={styles.container}>
      //   <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='   Customer Name' placeholderTextColor='skyblue' autoCapitalize='none' onChangeText={this.handleName} />
      //   <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='   Customer Email' placeholderTextColor='skyblue' autoCapitalize='none' onChangeText={this.handleEmail} />
      //   <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='   Customer Phone' placeholderTextColor='skyblue' autoCapitalize='none' onChangeText={this.handlePhone} />

      //   <Invoice data={{ customername, customerphone, customeremail }} />

      //   {/* <TouchableOpacity style={styles.submitButton} onPress={() => this.login(this.state.customername, this.state.customerphone)}>
      //     <Text style={styles.submitButtonText}> Submit </Text>
      //   </TouchableOpacity> */}
      // </View>
      //   <KeyboardAvoidingView style={styles.containerView} behavior='padding'>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.loginScreenContainer}>
          <View style={styles.loginFormView}>
            <Text style={styles.LoginFormText}> Username</Text>
            <TextInput
              value={this.state.givenName}
              placeholderColor="#c4c3cb"
              placeholderTextColor="#000"
              style={styles.loginFormTextInput}
              onChangeText={(value) => this.handleUserName(value)}
            />
            <Text style={styles.LoginFormText}> First Name</Text>
            <TextInput
              value={this.state.name}
              placeholderColor="#c4c3cb"
              placeholderTextColor="#000"
              style={styles.loginFormTextInput}
              onChangeText={(value) => this.handleFirstName(value)}
            />
            <Text style={styles.LoginFormText}> Last Name</Text>
            <TextInput
              value={this.state.familyName}
              placeholderColor="#c4c3cb"
              placeholderTextColor="#000"
              style={styles.loginFormTextInput}
              onChangeText={(value) => this.handleLastName(value)}
            />
            <Text style={styles.LoginFormText}> Phone Number</Text>
            <TextInput
              value={this.state.customerphone}
              placeholder="Enter Phone"
              placeholderColor="#c4c3cb"
              placeholderTextColor="#c4c3cb"
              style={styles.loginFormTextInput}
              onChangeText={(value) => this.handlePhone(value)}
              keyboardType="number-pad"
            />
            {/* <View>
              <Button title='Save Changes' onPress={this.saveChangesPressed} style={styles.loginButton} />
            </View> */}
            <View style={{ marginTop: 10 }}>
              <TouchableHighlight
                style={{
                  shadowColor: lightTheme.WHITE_COLOR,
                  shadowOpacity: 0.26,
                  shadowOffset: { width: 0, height: 2 },
                  shadowRadius: 8,
                  elevation: 5,
                  width: "40%",
                  height: "10%",
                  padding: 20,
                  borderRadius: 10,
                  alignSelf: "center",
                  justifyContent: "center",
                  backgroundColor: lightTheme.SECONDARY_COLOR,
                }}
                onPress={this.saveChangesPressed}
              >
                <Text
                  style={{
                    ...FONTS.h5,
                    textAlign: "center",
                    color: lightTheme.WHITE_COLOR,
                  }}
                >
                  SAVE
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
export default EditProfile;

const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: "skyblue",
    borderWidth: 1,
    padding: 30,
  },
  submitButton: {
    backgroundColor: "skyblue",
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: "white",
  },

  containerView: {
    flex: 1,
  },
  loginScreenContainer: {
    flex: 1,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  logoText: {
    fontSize: 40,
    fontWeight: "800",
    marginTop: 150,
    marginBottom: 30,
    textAlign: "center",
    ...FONTS.h2,
  },
  loginFormView: {
    flex: 1,
    marginTop: 10,
  },
  LoginFormText: {
    marginLeft: 15,
    color: lightTheme.PRIMARY_TEXT_COLOR,
    ...FONTS.h4,
  },
  loginFormTextInput: {
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#eaeaea",
    backgroundColor: "#fafafa",
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 20,
  },
  loginButton: {
    backgroundColor: "#3897f1",
    borderRadius: 5,
    height: 45,
    marginTop: 10,
  },
  fbLoginButton: {
    height: 45,
    marginTop: 10,
    backgroundColor: "transparent",
  },
});
