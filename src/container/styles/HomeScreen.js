import { FONTS, lightTheme } from '../../constants';

export default {
  container: { flex: 1, backgroundColor: lightTheme.PRIMARY_BACKGROUND_COLOR },
  maintext: {
    ...FONTS.h4,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15,
  },
  textStyles: {
    textAlign: 'center',
    ...FONTS.h4,
    color: lightTheme.BLACK_COLOR,
    marginTop: 5,
  },
};
