import { FONTS, lightTheme } from '../../constants';

export default {
  container: {
    flex: 1,
    backgroundColor: lightTheme.PRIMARY_BACKGROUND_COLOR,
  },
  buttonStyles: {
    shadowColor: lightTheme.WHITE_COLOR,
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    width: '50%',
    borderRadius: 10,
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
  },
  buttonText: {
    textAlign: 'center',
    color: lightTheme.WHITE_COLOR,
    ...FONTS.h4,
  },
};
