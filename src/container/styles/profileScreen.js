import { Dimensions } from 'react-native';
import { DIMENSIONS, FONTS, lightTheme } from '../../constants/Theme';

const { width, height } = Dimensions.get('window');
export default {
  cardContainer: {
    width: DIMENSIONS.fullWidth * 0.85,
    height: DIMENSIONS.fullHeight * 0.45,
    backgroundColor: '#434340',
    justifyContent: 'center',
    elevation: 5,
    borderRadius: 30,
  },
  cardImageContainer: {
    // flex: 2,
    justifyContent: 'center',
  },
  textcontainerStyles: {
    flex: 1,
    // justifyContent: "center",
    // alignContent: "flex-start",
  },
  textStyles: {
    textAlign: 'center',
    ...FONTS.h5,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  ImageContainer: {
    alignSelf: 'center',
    width: '90%',
    height: '100%',
    justifyContent: 'center',
    flex: 2,
    alignItems: 'center',
  },
  FacultyStyles: {
    borderWidth: 3,
    borderColor: lightTheme.SECONDARY_TEXT_COLOR,
    position: 'absolute',
    width: 120,
    top: -80,
    height: 120,
    resizeMode: 'cover',

    borderRadius: 60,
    // elevation: 1,
  },
};
