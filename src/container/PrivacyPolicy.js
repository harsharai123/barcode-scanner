import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import PrivacyPolicyHeader from '../component/Header/PrivacyPolicyHeader';
import { FONTS, lightTheme } from '../constants';

class PrivacyPolicy extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <PrivacyPolicyHeader navigation={navigation} />,
    };
  };
  agreePressed = () => {
    this.props.navigation.navigate('ProfileScreen');
  };
  render() {
    return (
      <View style={styles.fullScreen}>
        <ScrollView>
          <Text style={{ margin: 10, ...FONTS.h5 }}>
            {' '}
            We only use your personal information to provide you the Contently services or to communicate with you about the services or the App. With respect to any documents you may choose to upload to Contently, we take the privacy and confidentiality of such documents seriously. We encrypt all documents, and permanently delete any redacted edits you make to documents. If you choose to make a document public, we recommend you redact any and
            all references to people and addresses, as we can’t protect public data and we are not responsible for any violation of privacy law you may be liable for. We employ industry standard techniques to protect against unauthorized access of data about you that we store, including personal information. We do not share personal information you have provided to us without your consent, unless: – doing so is appropriate to carry out your own
            request; – we believe it’s needed to enforce our Terms of Service, or that is legally required; – we believe it’s needed to detect, prevent or address fraud, security or technical issues; – otherwise protect our property, legal rights, or that of others.
          </Text>
        </ScrollView>
        <TouchableHighlight style={{ shadowColor: lightTheme.WHITE_COLOR, shadowOpacity: 0.26, shadowOffset: { width: 0, height: 2 }, shadowRadius: 8, elevation: 5, width: '40%', height: '10%', padding: 20, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', backgroundColor: lightTheme.SECONDARY_COLOR }} onPress={this.agreePressed}>
          <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>Agree</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  fullScreen: {
    margin: 10,
  },
});
export default PrivacyPolicy;
