import { Ionicons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { getTodaysBill } from "../../NewServices";
import { lightTheme } from "../constants";
import { FONTS } from "../constants/Theme";
const { width, height } = Dimensions.get("window");

const TodaysBill = (props) => {
  const [reports, setReports] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  useEffect(() => {
    fetchReport();
  }, []);
  const fetchReport = async () => {
    try {
      const val = await AsyncStorage.getItem("logincredential");
      const userDetails = JSON.parse(val);
      const resp = await getTodaysBill({
        userId: userDetails.id,
      });

      console.log("data fetched Success:", resp);
      setReports(resp.Reports);
      setTotalAmount(resp.TotalAmount.totalAmount);
    } catch (error) {
      setReports([]);
      setTotalAmount(0);
      console.log("data fetched Error: ", error);
    }
  };

  const homeScreen = () => {
    props.navigation.navigate("HomeScreen");
  };

  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <StatusBar color="#fff" />
        <Ionicons
          name="arrow-back-circle-outline"
          size={30}
          color={lightTheme.PRIMARY_TEXT_COLOR}
          style={styles.icon}
          onPress={homeScreen}
        />
        <Text style={styles.headerText}>Today's Transaction</Text>
      </View>
      <View style={styles.totalamountCard}>
        <View style={styles.topLeftContainerTotalAmount}>
          <Text style={{ ...FONTS.h4 }}>Total Transaction: </Text>
        </View>
        <View style={{ marginRight: 20, alignItems: "center" }}>
          <Text style={{ fontSize: 10, ...FONTS.h4, color: "#282828" }}>
            {" "}
            ₹{totalAmount}
          </Text>
        </View>
      </View>

      <View>
        <ScrollView>
          <View style={{ height: 30 }} />
          {reports?.length !== 0 ? (
            <View>
              {reports?.map((ele, i) => (
                <View key={i} style={styles.cartCard}>
                  {/* <Image source={item.image} style={{ height: 80, width: 80 }} /> */}
                  <View style={styles.topLeftContainer}>
                    <Text style={{ ...FONTS.h5, marginTop: 12 }}>
                      Invoice # : {ele.invoiceId}
                    </Text>
                    <Text
                      style={{ fontSize: 13, marginTop: 5, color: "#282828" }}
                    >
                      Date :{ele.createdDate}
                    </Text>
                    <Text
                      style={{ fontSize: 13, marginTop: 5, color: "#282828" }}
                    >
                      Scanned By : {ele.userName}
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          ...FONTS.h4,
                          textAlign: "center",
                          alignSelf: "center",
                        }}
                      >
                        {" "}
                      </Text>
                      <TextInput
                        style={{
                          ...FONTS.h4,
                          color: "#282828",
                          justifyContent: "center",
                        }}
                      />
                    </View>
                    {/* <Text style={{ ...FONTS.h4 }}>${item.Price}</Text> */}
                  </View>
                  <View style={{ marginRight: 20, alignItems: "center" }}>
                    <Text
                      style={{ fontSize: 10, ...FONTS.h4, color: "#282828" }}
                    >
                      ₹{ele.billAmount}
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          ) : (
            <View>
              <Image
                source={require("../../assets/Icons.jpg")}
                style={{ resizeMode: "cover", width: "100%", height: "90%" }}
              />
              <Text
                style={{
                  ...FONTS.h4,
                  textAlign: "center",
                  color: lightTheme.BLACK_COLOR,
                }}
              >
                Whoops...No Transactions done today!
              </Text>
            </View>
          )}
          <View style={{ height: 300 }} />
        </ScrollView>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  dateDisplayer: {
    flexDirection: "row",
    // justifyContent: 'space-between',
  },
  container: {
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
    flex: 1,
  },
  Header: {
    height: "8.2%",
    alignItems: "center",
    backgroundColor: lightTheme.BLACK_COLOR,
    flexDirection: "row",
  },
  afterHeader: {
    padding: 5,
    marginTop: 1,
    flexDirection: "row",

    alignItems: "center",
    justifyContent: "space-between",
  },
  startButton: {
    shadowColor: lightTheme.WHITE_COLOR,
    shadowOpacity: 0.26,
    padding: 20,
    borderRadius: 10,
    marginTop: 10,
    height: 15,

    shadowRadius: 8,
    elevation: 5,
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: lightTheme.SECONDARY_COLOR,
    alignItems: "center",
  },
  scrollView: {
    backgroundColor: "#CCCCCC",
  },
  icon: {
    alignItems: "center",
    marginLeft: 15,
  },
  headerText: {
    ...FONTS.h2,

    marginLeft: 10,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  billcardImageStyles: {
    width: 80,
    height: 80,
  },
  header: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: lightTheme.WHITE_COLOR,
  },

  loginFormTextInput: {
    height: 33,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: lightTheme.BLACK_COLOR,
    backgroundColor: "#fafafa",
    paddingLeft: 10,
    marginLeft: 5,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  cartCard: {
    height: 100,
    elevation: 15,
    borderRadius: 10,
    borderColor: "green",
    borderWidth: 1,
    backgroundColor: "#FFF",
    marginVertical: 5,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  totalamountCard: {
    height: 90,
    elevation: 15,
    borderRadius: 10,
    borderColor: "blue",
    borderWidth: 1,
    backgroundColor: "#FFF",
    marginVertical: 10,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  actionBtn: {
    width: 80,
    height: 30,
    backgroundColor: lightTheme.SECONDARY_COLOR,
    borderRadius: 30,
    paddingHorizontal: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  topLeftContainer: {
    height: 100,
    marginLeft: 10,
    paddingVertical: 5,
    flex: 1,
  },
  topLeftContainerTotalAmount: {
    // height: 100,
    // marginLeft: 10,
    ...FONTS.h2,
    paddingVertical: 20,
    flex: 1,

    alignItems: "center",
  },

  card: {
    margin: 10,
    height: 0.5,
    width: 300,
  },
});
export default TodaysBill;
