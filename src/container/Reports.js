import { AntDesign, Ionicons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { useState } from "react";
import {
  Dimensions,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { getBillByDate } from "../../NewServices";
import FullScreenLoader from "../component/FullScreenLoader";
import { lightTheme } from "../constants";
import { FONTS } from "../constants/Theme";

const { width, height } = Dimensions.get("window");
const Reports = (props) => {
  const [data, setData] = useState([]);
  const [startDate, setstartDate] = useState(null);
  const [endDate, setendDate] = useState(null);
  const [datePicker, setdatePicker] = useState(false);
  const [loader, setloader] = useState(false);

  const handleConfirm = (date) => {
    const crtdate = moment(date).format("DD/MM/yyyy");
    console.log("A date has been picked: ", crtdate);
    if (datePicker == "START_DATE") setstartDate(crtdate);
    else if (datePicker == "END_DATE") setendDate(crtdate);
    setdatePicker(false);
  };
  const homeScreen = () => {
    props.navigation.navigate("HomeScreen");
  };
  const pressButton = () => {
    fetchBill();
  };
  const fetchBill = async () => {
    if (startDate && endDate) {
      setloader(true);
      try {
        const val = await AsyncStorage.getItem("logincredential");
        const userDetails = JSON.parse(val);

        const resp = await getBillByDate({
          fromDate: startDate,
          toDate: endDate + "T23:59:59",
          userId: userDetails.id,
        });
        console.log("Report Success:", resp);
        setData(resp);
        setloader(false);
      } catch (error) {
        console.log("data fetched Error: ", error);
        setloader(false);
      }
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <StatusBar color="#fff" />
        <Ionicons
          name="arrow-back-circle-outline"
          size={30}
          color={lightTheme.PRIMARY_TEXT_COLOR}
          style={styles.icon}
          onPress={homeScreen}
        />
        <Text style={styles.headerText}>Reports</Text>
      </View>
      <View style={styles.afterHeader}>
        {/* <View style={{ flexDirection: 'row' }}>
          <TouchableHighlight style={styles.startButton} title='Start Date'>
            <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>Start Date</Text>
          </TouchableHighlight>
        </View> */}
        {/* <View>
          <TouchableHighlight style={styles.endDateButton} title='End Date' onPress={() => setdatePicker('END_DATE')}>
            <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>End Date</Text>
          </TouchableHighlight>
        </View> */}
      </View>
      <DateTimePickerModal
        isVisible={datePicker ? true : false}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={() => setdatePicker(false)}
      />
      {/* <View style={{ flexDirection: 'row', marginTop: 5 }}> */}
      <View style={styles.dateDisplayer}>
        <AntDesign
          style={{ marginTop: 5, marginLeft: 10 }}
          name="calendar"
          size={34}
          color="#282828"
        />
        <TextInput
          value={startDate}
          caretHidden={true}
          placeholderTextColor="black"
          placeholderColor="#c4c3cb"
          width={120}
          style={styles.loginFormTextInput}
          onFocus={() => setdatePicker("START_DATE")}
        />
        {/* <AntDesign style={{ marginTop: 5, marginLeft: 1 }} name='calendar' size={34} color='#282828' /> */}

        {/* <View style={{ marginLeft: 50 }}> */}
        <AntDesign
          style={{ marginTop: 5, marginLeft: 1 }}
          name="calendar"
          size={34}
          color="#282828"
        />
        <TextInput
          value={endDate}
          caretHidden={true}
          placeplaceholderTextColor="black"
          placeholderColor="#c4c3cb"
          width={120}
          style={styles.loginFormTextInput}
          onFocus={() => setdatePicker("END_DATE")}
        />
        {/* <AntDesign style={{ marginTop: 5, marginLeft: 1 }} name='calendar' size={34} color='#282828' /> */}
      </View>
      {/* </View> */}
      <View
        style={{
          flexDirection: "column",
          justifyContent: "center",
          marginTop: 10,
          marginBottom: 10,
        }}
      >
        <TouchableHighlight
          style={styles.startButton}
          title="Start Date"
          color="black"
          onPress={() => pressButton()}
        >
          <Text
            style={{
              ...FONTS.h4,
              textAlign: "center",
              color: lightTheme.WHITE_COLOR,
            }}
          >
            Search{" "}
          </Text>
        </TouchableHighlight>
        {/* <Button title='Select Date' color='#3366FF' accessibilityLabel='Learn more about this purple button' /> */}
      </View>
      <View>
        {loader && <FullScreenLoader />}
        <ScrollView>
          <View style={{ height: 30 }} />
          {data.length !== 0 ? (
            <View>
              {data.map((ele, i) => (
                <View key={i} style={styles.cartCard}>
                  {/* <Image source={item.image} style={{ height: 80, width: 80 }} /> */}
                  <View style={styles.topLeftContainer}>
                    <Text style={{ ...FONTS.h5, marginTop: 12 }}>
                      Invoice #: {ele.invoiceId}
                    </Text>
                    <Text
                      style={{ fontSize: 13, marginTop: 5, color: "#282828" }}
                    >
                      Date: {ele.createdDate}
                    </Text>
                    <Text
                      style={{ fontSize: 13, marginTop: 5, color: "#282828" }}
                    >
                      Scanned By : {ele.userName}
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          ...FONTS.h4,
                          textAlign: "center",
                          alignSelf: "center",
                        }}
                      >
                        {" "}
                      </Text>
                      <TextInput
                        style={{
                          ...FONTS.h4,
                          color: "#282828",
                          justifyContent: "center",
                        }}
                      />
                    </View>
                    {/* <Text style={{ ...FONTS.h4 }}>${item.Price}</Text> */}
                  </View>
                  <View style={{ marginRight: 20, alignItems: "center" }}>
                    <Text
                      style={{ fontSize: 10, ...FONTS.h4, color: "#282828" }}
                    >
                      ₹{ele.billAmount}
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          ) : (
            <View>
              <Image
                source={require("../../assets/Icons.jpg")}
                style={{ resizeMode: "cover", width: "100%", height: "90%" }}
              />
              <Text
                style={{
                  ...FONTS.h4,
                  textAlign: "center",
                  color: lightTheme.BLACK_COLOR,
                }}
              >
                Please search for the reports!
              </Text>
            </View>
          )}

          <View style={{ height: 300 }} />
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dateDisplayer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  container: {
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
    flex: 1,
  },
  Header: {
    height: "8.2%",
    alignItems: "center",
    backgroundColor: lightTheme.BLACK_COLOR,
    flexDirection: "row",
  },
  afterHeader: {
    padding: 5,
    marginTop: 1,
    flexDirection: "row",

    alignItems: "center",
    justifyContent: "space-between",
  },
  startButton: {
    shadowColor: lightTheme.WHITE_COLOR,
    shadowOpacity: 0.26,
    padding: 20,
    borderRadius: 10,
    marginTop: 10,
    height: 15,

    shadowRadius: 8,
    elevation: 5,
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: lightTheme.SECONDARY_COLOR,
    alignItems: "center",
  },
  endDateButton: {
    shadowColor: lightTheme.WHITE_COLOR,
    shadowOpacity: 0.26,
    padding: 20,
    borderRadius: 10,
    marginTop: 10,
    height: 15,

    shadowRadius: 8,
    elevation: 5,

    backgroundColor: lightTheme.SECONDARY_COLOR,
    alignItems: "center",
  },
  icon: {
    alignItems: "center",
    marginLeft: 15,
  },
  cardContainer: {
    flex: 1,
    backgroundColor: "#F8F8F8",
    margin: 15,
    borderRadius: 25,
    paddingVertical: 80,
  },
  headerText: {
    ...FONTS.h2,

    marginLeft: 10,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  billcardImageStyles: {
    width: 80,
    height: 80,
  },
  header: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: lightTheme.WHITE_COLOR,
  },

  loginFormTextInput: {
    height: 33,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: lightTheme.BLACK_COLOR,
    backgroundColor: "#fafafa",
    paddingLeft: 10,
    marginLeft: 5,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  cartCard: {
    height: 100,
    elevation: 15,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "green",
    backgroundColor: "#FFF",
    marginVertical: 10,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  actionBtn: {
    width: 80,
    height: 30,
    backgroundColor: lightTheme.SECONDARY_COLOR,
    borderRadius: 30,
    paddingHorizontal: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  topLeftContainer: {
    height: 100,
    marginLeft: 10,
    paddingVertical: 5,
    flex: 1,
  },

  card: {
    margin: 10,
    height: 0.5,
    width: 300,
  },
});
export default Reports;
