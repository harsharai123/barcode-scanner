import { Ionicons } from '@expo/vector-icons';
import * as MediaLibrary from 'expo-media-library';
import React, { Component } from 'react';
import { Dimensions, FlatList, Image, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Barcode from 'react-native-barcode-expo';
import { captureRef } from 'react-native-view-shot';
import { connect } from 'react-redux';
import { FONTS, lightTheme } from '../constants';
const width_proportion = '80%';
const { width, height } = Dimensions.get('window');
const sleep = ms =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
class AllItems extends Component {
  state = {
    itemData: this.props.items,
    getItems: [],
    filteredItems: [],
    downloadItem: null,
    refreshing: false,
  };
  r2Ref = React.createRef();
  componentDidMount = () => {
    var arrSor = [];
    refreshing: false;
    // this.props.items.forEach(function (el) {
    //   if (el.barCodeId === null) {
    //     el.enableButton = false;
    //     arrSor.unshift(el);
    //   } else {
    //     el.enableButton = false;
    //     arrSor.push(el);
    //   }
    // });
    // console.log('SORTED ITEM LISTS', arrSor);
    this.setState({
      getItems: this.props.items,
      filteredItems: this.props.items,
      refreshing: false,
    });
  };

  onSearch = e => {
    let text = e.toLowerCase();
    let trucks = this.state.getItems;
    let filteredName = trucks.filter(item => {
      return item.itemName?.toLowerCase().match(text);
    });
    if (!text || text === '') {
      this.setState({
        filteredItems: trucks,
      });
    } else if (!Array.isArray(filteredName) && !filteredName.length) {
      // set no data flag to true so as to render flatlist conditionally
      this.setState({
        filteredItems: [],
      });
    } else if (Array.isArray(filteredName)) {
      this.setState({
        filteredItems: filteredName,
      });
    }
  };
  downloadClicked = async item => {
    const permission = await MediaLibrary.requestPermissionsAsync();
    if (permission.granted) {
      try {
        await this.setState({ downloadItem: item });
        await sleep(100);
        const result = await captureRef(this.r2Ref, {
          quality: 1,
          format: 'png',
        });
        const asset = await MediaLibrary.createAssetAsync(result);
        try {
          await MediaLibrary.createAlbumAsync('Barcodes', asset, false);
          alert('File Saved Successfully!');
        } catch (err) {
          alert('Error In Saving File!');
        }
      } catch (error) {
        console.log(error);
      } finally {
        this.setState({ downloadItem: null });
      }
    } else {
      alert('Need Storage permission to save file');
    }
  };
  onPressCard = item => {
    console.log('ITEM PRESSED', item);
    this.props.navigation.navigate('AddItemsBarcodeScan', {
      ItemDetails: item.itemCode,
    });
  };
  onPressCreateCard = item => {
    console.log('ITEM PRESSED', item);
    this.props.navigation.navigate('CreateBarcode', {
      ItemDetails: item.itemCode,
      ItemName: item.itemName,
      ItemCost: item.mrp,
    });
  };
  HomePressed = () => {
    this.props.navigation.navigate('HomeScreen');
  };
  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true,
        seed: this.state.seed + 1,
      },
      () => {
        this.componentDidMount();
      }
    );
  };

  renderItem = ({ item }) => (
    <View>
      {/* <View style={styles.Afterheader}> */}
      <TouchableOpacity style={styles.cartCard} disabled={true}>
        <View style={styles.topLeftContainer}>
          <Text style={{ ...FONTS.h7, color: '#282828' }}>Name : {item.itemName}</Text>
          <View style={{ flexDirection: 'row', height: 10, width: 15, color: '#282828' }}>
            <Image style={{ height: 120, width: 100, marginLeft: 180 }} source={{ uri: item.imageUrl }} />
          </View>

          <Text style={{ ...FONTS.h5, marginTop: 4, color: '#282828' }}>Metal : {item.metal}</Text>
          <Text style={{ ...FONTS.h5, marginTop: 4, color: '#282828' }}>Purity : {item.purity}</Text>
          <Text style={{ ...FONTS.h5, marginTop: 4, color: '#282828' }}>Total Pieces : {item.totalPieces}</Text>
          <Text style={{ ...FONTS.h5, marginTop: 4, color: '#282828' }}>Weight : {item.weight}gm</Text>
          <Text style={{ ...FONTS.h5, marginTop: 4, color: '#282828' }}>MC Type : {item.mcType}</Text>
        </View>

        {/* <Image source={(image === null) ? defaultImage : { uri: image }} /> */}
        <View style={{ marginRight: 20, alignSelf: 'center' }}>{/* <Text style={{ ...FONTS.h4, textAlign: 'center', alignSelf: 'center' }}>₹{item.mrp}</Text> */}</View>
      </TouchableOpacity>
    </View>
  );

  render() {
    const { downloadItem } = this.state;
    return (
      <View style={styles.container}>
        {/* {!this.props.items.length && <FullScreenLoader />} */}
        <View style={styles.header}>
          <StatusBar color='#fff' />

          <Ionicons name='arrow-back-circle-outline' style={styles.icon} size={32} justifyContent='space-between' alignSelf='flex-start' position='absolute' color='#CCCCCC' onPress={this.HomePressed} />
          <TextInput
            style={{
              height: 24,
              marginTop: 0,
              marginLeft: 10,
              marginRight: 10,
              width: width * 0.7,
              borderColor: '#CCCCCC',
              backgroundColor: 'white',
              paddingLeft: 5,
              paddingRight: 5,
            }}
            placeholderColor='white'
            placeholderTextColor='#c4c3cb'
            onChangeText={this.onSearch}
          />

          {/* <Ionicons name='md-search-circle-outline' size={32} justifyContent='space-between' alignSelf='flex-end' position='absolute' color='#CCCCCC' /> */}
        </View>

        {this.state.itemData.length == 0 && <Text style={{ ...FONTS.h5, color: 'black' }}>Not Found</Text>}

        <FlatList data={this.state.filteredItems} backgroundColor={lightTheme.PRIMARY_TEXT_COLOR} renderItem={this.renderItem} keyExtractor={(item, index) => `${item.id}`} refreshing={this.state.refreshing} onRefresh={this.handleRefresh} />

        <View style={{ height: 600 }} />
        {downloadItem && (
          <View collapsable={false} style={styles.barcodeStyle} ref={this.r2Ref}>
            {!!downloadItem.barCodeId && <Barcode value={downloadItem.barCodeId} format='CODE128' text={downloadItem.barCodeId} />}
            <View style={styles.textOfbarcode}>
              <Text style={{ marginLeft: 10, ...FONTS.h5 }}>Name: {downloadItem.itemName}</Text>
              <Text style={{ marginLeft: 10, ...FONTS.h5 }}>MRP : ₹{downloadItem.mrp}</Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
  },
  buttonView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    marginTop: 30,
  },
  textOfbarcode: {
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  header: {
    height: '4.7%',

    alignItems: 'center',
    backgroundColor: lightTheme.BLACK_COLOR,
    flexDirection: 'row',
  },
  Afterheader: {
    backgroundColor: lightTheme.PRIMARY_TEXT_COLOR,
  },

  startButton: {
    shadowColor: lightTheme.WHITE_COLOR,
    shadowOpacity: 0.26,
    padding: 20,
    borderRadius: 10,
    marginTop: 10,
    height: 15,

    shadowRadius: 8,
    elevation: 5,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: lightTheme.SECONDARY_COLOR,
    alignItems: 'center',
  },
  barcodeStyle: {
    marginLeft: 10,
    marginRight: 10,
  },

  cardContainer: {
    flex: 1,
    backgroundColor: '#F8F8F8',
    margin: 15,
    borderRadius: 25,
    //  paddingVertical: 80,
  },
  billcardImageStyles: {
    width: 80,
    height: 80,
  },
  icon: {
    alignItems: 'center',
    marginLeft: 15,
  },

  loginFormTextInput: {
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  cartCard: {
    height: height - 560,
    elevation: 15,
    borderRadius: 10,
    backgroundColor: '#FFF',
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: 'row',
    // alignItems: "center",
  },
  actionBtn: {
    width: 80,
    height: 30,
    backgroundColor: lightTheme.SECONDARY_COLOR,
    borderRadius: 30,
    paddingHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  topLeftContainer: {
    height: 180,
    marginLeft: 10,
    // paddingVertical: 20,
    flex: 1,
  },

  card: {
    margin: 10,
    height: 0.5,
    width: 300,
  },
});
{
  /* <View style={{ height: 30 }} />
          {this.props.items.map((item, i) => ( */
}

const mapStateToProps = state => ({
  items: state.items,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AllItems);
