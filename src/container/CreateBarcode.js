import AsyncStorage from '@react-native-async-storage/async-storage';
import * as MediaLibrary from 'expo-media-library';
import React, { Component } from 'react';
import { Alert, Dimensions, Keyboard, KeyboardAvoidingView, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import Barcode from 'react-native-barcode-expo';
import { captureRef } from 'react-native-view-shot';
import { connect } from 'react-redux';
import { getBarcodeNumber, postBarcodeNumber } from '../../NewServices';
import CreateBarcodeHeader from '../component/Header/CreateBarcodeHeader';
import { FONTS, lightTheme } from '../constants';
import { updateItem } from '../store/actions/itemActions';

const { width, height } = Dimensions.get('window');
const isNeedErrorPopup = true;
const errPopup = err => isNeedErrorPopup && Alert.alert('Error', err.message, [{ text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' }]);
const connected = comp => connect(state => ({}), { updateItem })(comp);
export default connected(
  class CreateBarcode extends Component {
    static navigationOptions = ({ navigation }) => {
      return {
        headerTitle: () => <CreateBarcodeHeader navigation={navigation} />,
      };
    };
    constructor(props) {
      super(props);
      this.state = {
        userData: {},
        isBarCodeCreated: '',
        barcodeNumber: '',
        loading: false,
        ItemCode: props.navigation.state.params.ItemDetails,
        ItemName: props.navigation.state.params.ItemName,
        ItemMrp: props.navigation.state.params.ItemCost,
      };
    }
    printPressed = () => {
      alert('This functionality is in progress,Please use Download option!');
    };
    r2Ref = React.createRef();
    componentDidMount = async () => {
      const val = await AsyncStorage.getItem('logincredential');
      const obj = JSON.parse(val);
      console.log('OBJJ', obj);
      await this.setState({ userData: obj, loading: true });
      try {
        const barcodeNumber = await getBarcodeNumber({
          userId: obj.id,
        });
        console.log('BARCODENUMBER', barcodeNumber);
        this.setState({ barcodeNumber: barcodeNumber.toString(), loading: false });
      } catch (err) {
        errPopup(err);
      }
    };
    downloadClicked = async () => {
      const permission = await MediaLibrary.requestPermissionsAsync();
      if (permission.granted) {
        try {
          const result = await captureRef(this.r2Ref, { quality: 1, format: 'png' });
          const asset = await MediaLibrary.createAssetAsync(result);
          try {
            await MediaLibrary.createAlbumAsync('Barcodes', asset, false);
            alert('File Saved Successfully!');
          } catch (err) {
            alert('Error In Saving File!');
          }
        } catch (error) {
          console.log(error);
        }
      } else {
        alert('Need Storage permission to save file');
      }
    };

    AddbarcodeToDatabase = async () => {
      this.props.updateItem(this.state.ItemCode, this.state.barcodeNumber, this.state.isBarCodeCreated);
      try {
        await postBarcodeNumber({
          userId: this.state.userData.id,
          itemCode: this.state.ItemCode,
          barcodeId: this.state.barcodeNumber,
        });

        this.setState({ printButton: true });
        alert(`Bar code added successfully!!!!`);
      } catch (err) {
        errPopup(err);
      }
    };

    render() {
      const { loading, userData, barcodeNumber, isBarCodeCreated, printButton } = this.state;

      return (
        <KeyboardAvoidingView style={styles.containerView} behavior='padding'>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.loginScreenContainer}>
              {!loading && (
                <View style={styles.loginFormView}>
                  {/* <Text style={styles.customerTexts}>Enter Barcode ID</Text> */}
                  {/* <TextInput placeholder='Barcode ID' placeholderColor='#c4c3cb' style={styles.loginFormTextInput} /> */}
                  <View collapsable={false} style={styles.barcodeStyle} ref={this.r2Ref}>
                    {!!barcodeNumber && <Barcode value={barcodeNumber} format='CODE128' text={barcodeNumber} />}
                    <View style={styles.textOfbarcode}>
                      <Text style={{ marginLeft: 10, ...FONTS.h5 }}>Name: {this.state.ItemName}</Text>
                      <Text style={{ marginLeft: 10, ...FONTS.h5 }}>MRP : ₹{this.state.ItemMrp}</Text>
                    </View>
                  </View>

                  {!printButton && (
                    <TouchableOpacity onPress={this.AddbarcodeToDatabase} style={{ shadowColor: lightTheme.WHITE_COLOR, shadowOpacity: 0.26, shadowOffset: { width: 0, height: 2 }, shadowRadius: 8, elevation: 2, width: 90, height: 40, padding: 10, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', marginTop: 10, marginTop: 10, backgroundColor: lightTheme.SECONDARY_COLOR }}>
                      <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>Save </Text>
                    </TouchableOpacity>
                  )}
                  <View style={styles.print}>
                    {printButton && (
                      <TouchableOpacity onPress={this.printPressed} style={{ shadowColor: lightTheme.WHITE_COLOR, shadowOpacity: 0.26, shadowOffset: { width: 0, height: 2 }, shadowRadius: 8, elevation: 2, width: 90, height: 40, padding: 10, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', marginTop: 10, backgroundColor: lightTheme.SECONDARY_COLOR }}>
                        <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>Print </Text>
                      </TouchableOpacity>
                    )}
                    {printButton && (
                      <TouchableOpacity onPress={this.downloadClicked} style={{ shadowColor: lightTheme.WHITE_COLOR, shadowOpacity: 0.26, shadowOffset: { width: 0, height: 2 }, shadowRadius: 8, elevation: 2, width: 90, height: 40, padding: 10, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', marginTop: 10, backgroundColor: lightTheme.SECONDARY_COLOR }}>
                        <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>Download </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              )}
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      );
    }
  }
);

const styles = StyleSheet.create({
  customerTexts: {
    marginLeft: 15,
    marginTop: 15,
    ...FONTS.h4,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  barcodeStyle: {
    marginLeft: 10,
    marginRight: 10,
  },
  container: {
    paddingTop: 23,
  },
  print: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 20,
  },
  textOfbarcode: {
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: 'skyblue',
    borderWidth: 1,
    padding: 30,
  },
  submitButton: {
    backgroundColor: 'skyblue',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },

  containerView: {
    flex: 1,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  loginScreenContainer: {
    flex: 1,
    marginTop: 10,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  logoText: {
    fontSize: 40,
    fontWeight: '800',
    marginTop: 150,
    marginBottom: 30,
    textAlign: 'center',
    ...FONTS.h2,
  },
  loginFormView: {
    flex: 1,
  },
  loginFormTextInput: {
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 15,
  },
  loginButton: {
    backgroundColor: '#3897f1',
    borderRadius: 5,
    height: 45,
    marginTop: 10,
  },
  fbLoginButton: {
    height: 45,
    marginTop: 10,
    backgroundColor: 'transparent',
  },
});
