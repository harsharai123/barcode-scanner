import React, { Component } from 'react';
import { Keyboard, KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableWithoutFeedback, View } from 'react-native';
import { connect } from 'react-redux';
import CustomerDetailsHeader from '../component/Header/CustomerDetailsHeader';
import Invoice from '../component/invoice/Invoice';
import { FONTS, lightTheme } from '../constants';
import { clearCart } from '../store/actions/cartActions';

class CustomerDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <CustomerDetailsHeader navigation={navigation} />,
    };
  };

  state = {
    customername: '',
    customeremail: '',
    customerphone: '',
  };
  handleName = text => {
    this.setState({ customername: text });
  };
  handleEmail = text => {
    this.setState({ customeremail: text });
  };
  handlePhone = text => {
    this.setState({ customerphone: text });
  };
  login = (name, phone, email) => {
    alert('Customername: ' + name + ' Customeremail: ' + email + ' Customerphone: ' + phone);
  };
  invoiceClose = val => {
    this.props.clearCart();

    // this.props.updateItemPrice();
    // this.props.updateTax();
    // this.props.addProduct();
    this.props.navigation.navigate('HomeScreen');
  };
  render() {
    // const { customername, customeremail, customerphone } = this.state;
    const { total } = this.props.navigation.state.params;
    console.log('TOTALLL', total);
    const { customername, customeremail, customerphone } = this.state;

    return (
      // <View style={styles.container}>
      //   <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='   Customer Name' placeholderTextColor='skyblue' autoCapitalize='none' onChangeText={this.handleName} />
      //   <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='   Customer Email' placeholderTextColor='skyblue' autoCapitalize='none' onChangeText={this.handleEmail} />
      //   <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='   Customer Phone' placeholderTextColor='skyblue' autoCapitalize='none' onChangeText={this.handlePhone} />

      //   <Invoice data={{ customername, customerphone, customeremail }} />

      //   {/* <TouchableOpacity style={styles.submitButton} onPress={() => this.login(this.state.customername, this.state.customerphone)}>
      //     <Text style={styles.submitButtonText}> Submit </Text>
      //   </TouchableOpacity> */}
      // </View>
      <KeyboardAvoidingView style={styles.containerView} behavior='padding'>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.loginScreenContainer}>
            <View style={styles.loginFormView}>
              <Text style={styles.customerTexts}>Customer Name</Text>
              <TextInput placeholder='Customer Name' placeholderColor='#c4c3cb' style={styles.loginFormTextInput} onChangeText={this.handleName} />
              <Text style={styles.customerTexts}>Customer Email </Text>
              <TextInput placeholder='Customer Email ' placeholderColor='#c4c3cb' style={styles.loginFormTextInput} onChangeText={this.handleEmail} />
              <Text style={styles.customerTexts}>Customer Phone</Text>
              <TextInput placeholder='Customer Phone' placeholderColor='#c4c3cb' style={styles.loginFormTextInput} onChangeText={this.handlePhone} keyboardType='number-pad' />
              <Invoice data={{ customername, customerphone, customeremail, total }} onClose={this.invoiceClose} />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  customerTexts: {
    marginLeft: 15,
    marginTop: 15,
    ...FONTS.h4,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  container: {
    paddingTop: 23,
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: 'skyblue',
    borderWidth: 1,
    padding: 30,
  },
  submitButton: {
    backgroundColor: 'skyblue',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },

  containerView: {
    flex: 1,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  loginScreenContainer: {
    flex: 1,
    marginTop: 10,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  logoText: {
    fontSize: 40,
    fontWeight: '800',
    marginTop: 150,
    marginBottom: 30,
    textAlign: 'center',
    ...FONTS.h2,
  },
  loginFormView: {
    flex: 1,
  },
  loginFormTextInput: {
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  loginButton: {
    backgroundColor: '#3897f1',
    borderRadius: 5,
    height: 45,
    marginTop: 10,
  },
  fbLoginButton: {
    height: 45,
    marginTop: 10,
    backgroundColor: 'transparent',
  },
});
const mapStateToProps = state => ({
  cart: state.cart.cart,
  subtotal: state.cart.subtotal,
  tax: state.cart.tax,
  total: state.cart.total,
});

const mapDispatchToProps = {
  clearCart,
};
export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetails);
