import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
const CustomButton = props => {
  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => props.onPress?.()} style={styles.button}>
          <Image source={require('../../../assets/images/googlelogo.png')} style={styles.ImageIconStyle} />
          <View style={styles.SeparatorLine} />
          <Text style={styles.buttonText}>{props.title}</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default CustomButton;
const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderWidth: 0.5,
    paddingRight: 50,
    borderColor: '#fff',
    height: 40,
    width: 240,
    borderRadius: 5,
    margin: 5,
  },
  buttonText: {
    color: '#000000',
    marginLeft: 20,
    marginRight: 50,
  },
  ImageIconStyle: {
    paddingRight: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
  },
  SeparatorLine: {
    backgroundColor: '#000000',
    width: 1,
    height: 40,
  },
});
