import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
const FacebookButton = props => {
  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => props.onPress?.()} style={styles.button}>
          <Image source={require('../../../assets/images/Facebook_Login_Button.png')} style={styles.ImageIconStyle} />
          <View style={styles.SeparatorLine} />
          <Text style={styles.buttonText}>{props.title}</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default FacebookButton;
const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 30,
    backgroundColor: '#485a96',
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 40,
    width: 240,
    borderRadius: 5,
    margin: 5,
  },
  buttonText: {
    color: '#fff',
    marginLeft: 20,
    marginRight: 50,
  },

  ImageIconStyle: {
    paddingRight: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
  },
  SeparatorLine: {
    backgroundColor: '#ffffff',
    width: 1,
    height: 40,
  },
});
