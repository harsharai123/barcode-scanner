import React, { Component } from 'react';
import { Alert, BackHandler, FlatList, SafeAreaView, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Card from '../component/card/Card';
import HomeHeader from '../component/Header/HomeHeader';
import BarCodeScan from '../component/scanner/BarCodeScan';
import { DATA } from '../data/HomeScreenData';
import { addProduct, updateCartQuantity } from '../store/actions/cartActions';
import styles from './styles/HomeScreen';

class Homescreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <HomeHeader navigation={navigation} />,
    };
  };

  //   _didFocusSubscription;
  //   _willBlurSubscription;

  constructor(props) {
    super(props);
    // this._didFocusSubscription = props.navigation.addListener(
    //   "didFocus",
    //   (payload) =>
    //     BackHandler.addEventListener("hardwareBackPress", this.handleBackPress)
    // );
    this.state = {
      modalVisible: false,
      addsmodalVisible: false,
      dataValues: [],
      value: '',
      showPost: false,
      search: '',
      isLoading: true,
      page: 0,
      isFetching: false,
      isVisible: false,
    };
  }

  cardSelected = value => {
    console.log('Navigating', value);
    if (value == 'ALL ITEMS') {
      console.log('Navigating 2', value);
      this.props.navigation.navigate('AllItems');
    } else if (value == 'REPORTS') {
      this.props.navigation.navigate('Reports');
      console.log('Navigating 3', value);
      // this.setState({ barCodeScannerVisible: true });
    } else if (value == 'TRANSACTION') {
      this.props.navigation.navigate('TodaysBill');
      console.log('Navigating 4', value);
      // this.setState({ barCodeScannerVisible: true });
    } else if (value == 'SCANNER') {
      this.props.navigation.navigate('Scanner');
      console.log('Navigating 3', value);
      // this.setState({ barCodeScannerVisible: true });
    }

    /*Back Handler Functionality*/
    handleBackPress = () => {
      Alert.alert(
        'Do you want to Exit the App?',
        ' ',
        [
          {
            text: 'No',
            onPress: () => {
              console.log('Cancel Pressed');
            },
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => BackHandler.exitApp(),
          },
        ],
        { cancelable: false }
      );

      return true;
    };

    filterData = async (value, callback) => {
      console.log('Value in Array', value);
      var item = this.state.items.find(item => {
        console.log(item.barcode, value);
        item.barcode === value;
      });
      // console.log('Item', item);
      callback(item);
      return item;
    };

    /*scanned values are stored in store and navigated*/
    navigate = value => {};
    // let numCallbackRuns = [];
    // console.log('DES', value);
    // console.log('Ji');
    // this.state.Products.forEach(function (item) {
    //   if (item.barcode === value.data) {
    //     numCallbackRuns.push(item);
    //     console.log("numCallbackRuns: ", numCallbackRuns);
    //   }
    // });
    // var valued = [];
    // var data = this.state.Products.map(item => {
    //   // console.log('true', item.barcode, value.data);
    //   if (item.barcode == value.data) {
    //     if (this.props.cart.length >= 1) {
    //       this.props.cart.map(element => {
    //         if (element.barcode === item.barcode) {
    //           var quantity = item.quantity + 1;
    //           this.props.updateCartQuantity(item.barcode, quantity);
    //         } else {
    //           this.props.addProduct(item);
    //         }
    //       });
    //     } else {
    //       this.props.addProduct(item);
    //     }

    //     // valued.push(item);
    //     // console.log('Result', valued);
    //   }
    // });
    // this.setState({ barCodeScannerVisible: false });
    // this.props.addProduct(val);
    // this.props.navigation.navigate('Brand');
  };

  /*Fetches the all the products data */
  componentDidMount = async () => {
    // console.log('DATA', DATA);
    // console.log('DATA', Products);
    this.setState({ data: DATA });
    this.setState({ Products: this.props.items });
    // this.props.updateCartQuantity("8901764362309", "2");
    // let numCallbackRuns = [];

    // Products.forEach(function (item) {
    //   if (item.barcode === 8901764072277) numCallbackRuns.push(item);
    // });

    // console.log("numCallbackRuns: ", numCallbackRuns);
    // let itemB = Products.forEach((item) => {
    //   item.barcode === 8901764072277;
    // });
    // console.log("New cart", itemB);
    // this._willBlurSubscription = this.props.navigation.addListener(
    //   "willBlur",
    //   (payload) =>
    //     BackHandler.removeEventListener(
    //       "hardwareBackPress",
    //       this.handleBackPress
    //     )
    // );
    // });
  };

  //   componentWillUnmount() {
  //     clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
  //     this._didFocusSubscription && this._didFocusSubscription.remove();
  //     this._willBlurSubscription && this._willBlurSubscription.remove();
  //   }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.maintext}>Your Insights</Text>
        {this.state.barCodeScannerVisible ? (
          <View style={{ flex: 1 }}>
            <BarCodeScan handleNavigation={value => this.navigate(value)} />
          </View>
        ) : (
          <FlatList
            data={this.state.data}
            renderItem={({ item }) => (
              <Card
                image={item.image}
                imageStyles={{
                  padding: 30,
                  width: 10,
                  height: 10,
                  alignSelf: 'center',
                  alignItems: 'center',
                  alignContent: 'center',
                }}
                cardImageContainer={{
                  alignSelf: 'center',
                  backgroundColor: item.backgroundColor,
                  // padding: 3,
                  borderRadius: 25,
                }}
                textStyles={styles.textStyles}
                cardText={item.name}
                handleNavigation={() => {
                  this.navigate(value);
                }}
                onPressCard={() => this.cardSelected(item.name)}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            numColumns={2}
            onEndReachedThreshold={0.1}
            initialNumToRender={10}
          />
        )}
      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => ({
  cart: state.cart.cart,
  items: state.items,
});
// const ActionCreators = Object.assign({}, { addProduct });
// const mapDispatchToProps = (dispatch) => ({
//   actions: bindActionCreators(ActionCreators, dispatch),
// });

const mapDispatchToProps = { addProduct, updateCartQuantity };

// const mapStateToProps = (state) => ({
//   cart: state.cart,
// });
export default connect(mapStateToProps, mapDispatchToProps)(Homescreen);
