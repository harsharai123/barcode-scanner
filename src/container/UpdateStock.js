import React, { Component } from 'react';
import { Keyboard, KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableHighlight, TouchableWithoutFeedback, View } from 'react-native';
import { connect } from 'react-redux';
import { updateStock } from '../../NewServices';
import UpdateStockHeader from '../component/Header/UpdateStockHeader';
import { FONTS, lightTheme } from '../constants';
import { clearCart } from '../store/actions/cartActions';
import { updateItem } from '../store/actions/itemActions';
const connected = comp => connect(state => ({}), { updateItem })(comp);
export default connected(
  class UpdateStock extends Component {
    static navigationOptions = ({ navigation }) => {
      return {
        headerTitle: () => <UpdateStockHeader navigation={navigation} />,
      };
    };
    constructor(props) {
      super(props);
      this.state = {
        itemname: props.navigation.state.params.ItemName,
        itemCode: props.navigation.state.params.ItemCode,
        updatestock: '',
        Stock: props.navigation.state.params.stock,
      };
    }
    handleName = text => {
      this.setState({ itemname: text });
    };

    handleStock = text => {
      this.setState({ updatestock: text });
    };

    updatePressed = async () => {
      this.props.updateItem(this.state.itemCode, this.state?.barCodeId, this.state.updatestock);
      try {
        await updateStock({
          itemCode: this.state.itemCode,
          stock: this.state.updatestock,
        });

        this.setState({ printButton: true });
        alert(`Stock updated successfully!!!!`);
        this.props.navigation.navigate('AddStocks');
      } catch (err) {
        errPopup(err);
      }
    };

    render() {
      const { itemname, updatestock, Stock } = this.state;
      console.log('STOCK', Stock);
      const stocks = Stock === null ? 0 : Stock;
      return (
        <KeyboardAvoidingView style={styles.containerView} behavior='padding'>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.loginScreenContainer}>
              <View style={styles.loginFormView}>
                <Text style={styles.customerTexts}>Item Name</Text>
                <TextInput placeholder={this.state.itemname} TextInput caretHidden={true} placeholderTextColor='#000' placeholderColor='black' style={styles.loginFormTextInput} />
                {/* <Text style={styles.customerTexts}>Customer Email </Text>
              <TextInput placeholder='Customer Email ' placeholderColor='#c4c3cb' style={styles.loginFormTextInput} onChangeText={this.handleEmail} /> */}
                <Text style={styles.customerTexts}>Update Stock</Text>
                <TextInput placeholder={`${stocks}`} placeholderColor='#000' placeholderTextColor='#000' style={styles.loginFormTextInput} onChangeText={this.handleStock} keyboardType='number-pad' />
                <View style={{ marginTop: 10 }}>
                  <TouchableHighlight style={{ shadowColor: lightTheme.WHITE_COLOR, shadowOpacity: 0.26, shadowOffset: { width: 0, height: 2 }, shadowRadius: 8, elevation: 5, width: '40%', height: '10%', padding: 20, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', backgroundColor: lightTheme.SECONDARY_COLOR }} onPress={this.updatePressed}>
                    <Text style={{ ...FONTS.h5, textAlign: 'center', color: lightTheme.WHITE_COLOR }}>UPDATE</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      );
    }
  }
);

const styles = StyleSheet.create({
  customerTexts: {
    marginLeft: 15,
    marginTop: 15,
    ...FONTS.h4,
    color: lightTheme.PRIMARY_TEXT_COLOR,
  },
  container: {
    paddingTop: 23,
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: 'skyblue',
    borderWidth: 1,
    padding: 30,
  },
  submitButton: {
    backgroundColor: 'skyblue',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },

  containerView: {
    flex: 1,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  loginScreenContainer: {
    flex: 1,
    marginTop: 10,
    backgroundColor: lightTheme.BLACK_COLOR,
  },
  logoText: {
    fontSize: 40,
    fontWeight: '800',
    marginTop: 150,
    marginBottom: 30,
    textAlign: 'center',
    ...FONTS.h2,
  },
  loginFormView: {
    flex: 1,
  },
  loginFormTextInput: {
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  loginButton: {
    backgroundColor: '#3897f1',
    borderRadius: 5,
    height: 45,
    marginTop: 10,
  },
  fbLoginButton: {
    height: 45,
    marginTop: 10,
    backgroundColor: 'transparent',
  },
});
const mapStateToProps = state => ({
  cart: state.cart.cart,
  subtotal: state.cart.subtotal,
  tax: state.cart.tax,
  total: state.cart.total,
});

const mapDispatchToProps = {
  clearCart,
};
