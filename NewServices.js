import axios from "axios";
import { Buffer } from "buffer";
//const BASE_URL = 'http://ec2-65-2-75-156.ap-south-1.compute.amazonaws.com:8080';
// const BASE_URL = 'http://192.168.0.189:8080';
const BASE_URL = "https://testapi.epari.jewelretail.roupya.com/jewelshop";
const COMMONHEADERS = {
  username: "RoupyaDevelopers",
  password: " JewelleryStore",
};
const ENCCRED = new Buffer(
  COMMONHEADERS.username + ":" + COMMONHEADERS.password
).toString("base64");
const STATUS_CODES = {
  SUCCESS_WITH_CONTENT: 200,
  SUCCESS_CREATED: 201,
  SUCCESS_WITHOUT_CONTENT: 204,
  BAD_REQUEST: 400,
  NOT_FOUND: 404,
  FORBIDDEN: 403,
  CONFLICT: 409,
  INTERNALSERVER: 500,
};

const objQuery = (data) =>
  Object.entries(data)
    .map(
      ([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
    )
    .join("&");

export const addUser = async (data) => {
  try {
    console.log("DATA of Users", data);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `https://testapi.epari.jewelretail.roupya.com/jewelshop/signup`,
      method: "POST",
      data,
    });
    // console.log('Add user Success:', resp);
    return resp?.data;
  } catch (err) {
    console.log("Add user err: ", err);
    console.error(err);
    throw err;
  }
};

// http://localhost:8080/user?userId=null&loginType=Google
export const updateUser = async (data) => {
  try {
    console.log("put DATA of Users", data);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/foos/user?${objQuery(data)}`,
      method: "PUT",
      data,
    });
    // console.log('Put user Success:', resp);
    return resp?.data;
  } catch (err) {
    console.log("put user err: ", err);
    throw err;
  }
};

export const addBill = async (data) => {
  try {
    console.log("DATA of Bills gen", data);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/billing`,
      method: "POST",
      data,
    });
    console.log("add bil Success:", resp);
    return resp?.data;
  } catch (err) {
    console.log("bill added err: ", err);
    throw err;
  }
};
//getallitems
export const getItemsApi = async () => {
  try {
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/itemsEstimation`,
      method: "GET",
    });
    console.log("RESP", resp);
    return resp?.data;
  } catch (err) {
    console.log("Items fetched err: ", err);
    throw err;
  }
};

export const getBillByDate = async (data) => {
  try {
    console.log("get Tasks", data);
    console.log("url", `${BASE_URL}/billing?${objQuery(data)}`);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/billing?${objQuery(data)}`,
      method: "GET",
    });

    console.log("Reports fetched success", resp);
    return resp?.data;
  } catch (err) {
    console.log(err);
    alert("No reports found!");
    return [];
  }
};

// http://localhost:8080/search?itemNameOrCategoryOrSubcategory=Medicine Get search by item category
export const searchByItemCategory = async (dataSet) => {
  try {
    console.log("Search by itemcategory", dataSet);
    const data = { itemNameOrCategoryOrSubcategory: dataSet.userId };
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/search?${objQuery(data)}`,
      method: "GET",
    });
    // console.log('Get by category or sub category Success:', resp);
    return resp?.data;
  } catch (err) {
    console.log("Get by category or sub category ", err);
    throw err;
  }
};

// http://localhost:8080/updateBarCodeId?itemCode=3&barCodeId=12345678 put barcode id using barcode and item id
export const putBarcodeId = async (data) => {
  try {
    console.log("put barcode id using item code", data);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/updateBarCodeId?${objQuery(data)}`,
      method: "PUT",
      data,
    });
    console.log("Put api success with barcode", resp);
    return resp?.data;
  } catch (err) {
    console.log("Put api err ", err);
    throw err;
  }
};

// http://localhost:8080/item?barCodeId=12345678 get only barcode id to scan items
export const getBarcodeidDuringScan = async (barCodeId) => {
  try {
    console.log("get DATA of items", barCodeId);
    const data = { barCodeId };
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/item?${objQuery(data)}`,
      method: "GET",
    });
    // console.log('get products api using barcode success', resp);
    return resp?.data;
  } catch (err) {
    console.log("get api err ", err);
    throw err;
  }
};

export const getUser = async (data) => {
  try {
    console.log("put DATA of Users", data);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/user?${objQuery(data)}`,
      method: "GET",
      data,
    });
    // console.log('Put user Success:', resp);
    return resp?.data;
  } catch (err) {
    console.log("Get profile user err: ", err);
    throw err;
  }
};
// http://localhost:8080/generateBarcode?userId=1&loginType=google
// GNERATING BARCODE ID
export const getBarcodeNumber = async (data) => {
  try {
    console.log("get Barcode number of items", data);

    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/generateBarcode?${objQuery(data)}`,
      method: "GET",
      data,
    });
    console.log("barcode number generated", resp);
    return resp?.data;
  } catch (err) {
    console.log("get barcode  err ", err);
    throw err;
  }
};
// Adding Barcode number to database
// http://localhost:8080/add?itemCode=1006
export const postBarcodeNumber = async (data) => {
  try {
    console.log("Barcode updating", data);

    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/add?${objQuery(data)}`,
      method: "POST",
      data,
    });

    console.log("barcode numberupdatd to db", resp);
    return resp?.data;
  } catch (err) {
    console.log("barcode not updated to db  err ", err);
    throw err;
  }
};
// FETCHING TODAY'S REPORT
// http://localhost:8080/presentDateBillingReport?userId=1
export const getTodaysBill = async (data) => {
  try {
    console.log("get Todays Bill", data);
    console.log(
      "url",
      `${BASE_URL}/presentDateBillingReport?${objQuery(data)}`
    );
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/presentDateBillingReport`,
      method: "GET",
    });

    console.log("Todays billed fetched success", resp);
    return resp?.data;
  } catch (err) {
    console.log("mayur", err);
    return [];
  }
};
// http://localhost:8080/user?loginId=mk&loginType=Google
export const getUserDetails = async (data) => {
  try {
    console.log("get user Details", data);
    console.log("url", `${BASE_URL}/user?${objQuery(data)}`);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/user?${objQuery(data)}`,
      method: "GET",
    });

    console.log("get user fetched success", resp);
    return resp?.data;
  } catch (err) {
    console.log("GETUSERERR", err);
  }
};
export const updateStock = async (data) => {
  try {
    console.log("put stock", data);
    console.log("url", `${BASE_URL}/updateStock?${objQuery(data)}`);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/updateStock?${objQuery(data)}`,
      method: "PUT",
    });

    console.log("put stock success", resp);
    return resp?.data;
  } catch (err) {
    console.log(err);
  }
};
// stock lessening during billing
// http://3.109.128.150:8081/updateMultipleStock
export const lessenStock = async (data) => {
  console.log("CORRECT FORMAT", data);
  try {
    console.log("put barcode id using item code", data);
    const resp = await axios({
      headers: {
        Authorization: "Basic Um91cHlhRGV2ZWxvcGVyczpKZXdlbGxlcnlTdG9yZQ==",
      },
      url: `${BASE_URL}/updateMultipleStock`,
      method: "PUT",
      data,
    });
    console.log("Put api success during billing", resp);
    return resp?.data;
  } catch (err) {
    console.log("Put api err during billing", err);
    throw err;
  }
};
